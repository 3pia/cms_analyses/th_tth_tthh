# Infos on plots for thesis

## 4-classes model
```bash
law run MulticlassOptStitchedEvaluation --version 20230825_new_base_with_bTagNorm --stitch-version 20230825_stitched --recipe txhx --print-status 1
```

## 6-classes model
```bash
law run MulticlassOptStitchedEvaluation --version 20230825_new_base_with_bTagNorm --stitch-version 20230904_6classes_2nd_search --recipe txhx --group-override tt+j_splitted --print-status 1
```

## Plots 'n' Cards
```bash
law run PlotsCards --version 20230904_all_corrections_6classes --year 2018
```