# Code Execution Strategy

### Get Stitched Model and its Evaluation
1. Run `ArrayExporter`
2. Hyperparameter Search for DNN, loop over searches:
   1. Define search_space in `OptimizerPreparation` in `optimize.py`
   2. Run `MulticlassOptimizerPlot` (multiple search possible)
    ```bash
    law run MulticlassOptimizerPlot --version 20230825_new_base_with_bTagNorm --opt-version 20230828_6classes_1st_search --group-override tt+j_splitted --recipe txhx --n-initial 200 --n-parallel 100 --iterations 500 --workers 100 --remove-output 0,i,1
    ```
3. Run `MulticlassOptStitchedEvaluation`
   ```bash
   law run MulticlassOptStitchedEvaluation --version 20230825_new_base_with_bTagNorm --stitch-version 20230831_6classes_1st_search --analysis-choice th_tth_tthh --recipe txhx --workers 5
   ```

### Get Histograms and Datacards
1. Provide model path in `analysis.py`
2. Run `Processor`
3. Run `PlotsCards`
   ```bash
   law run PlotsCards --version VERSION_NAME --year 2018
   ```

## Combine on LXPLUS

### find best parameters
```bash
law run CompareLikelihoodsTask --version 20230829_all_corrections_4classes --binning equal_frequency_sig§equal_frequency_bkg§equal_width§threshold --regions incl§tH+ttH --variable dnn_score_max --points 350 --scan-range 10 --workers 14 --remove-output 2,a,1
law run CompareLikelihoodsTask --version 20230904_all_corrections_6classes --binning equal_frequency_sig§equal_frequency_bkg§equal_width§threshold --regions incl§tH+ttH --variable dnn_score_max --points 350 --scan-range 10 --workers 10
```

### Uncertainties


### Signal Strength Expected Upper Limit WRONG
```bash
law run PlotUpperLimitsTask --version 20230904_all_corrections_6classes --variable dnn_score_max --points 200 --binning threshold --xsec-unit fb --x-min -4 --x-max 4 --remove-output 0,a,1
```

# Single Upper Limit Task
```bash
law run SingleUpperLimitTask --version 20230904_all_corrections_6classes --region tH+ttH --variable dnn_score_max --binning threshold
law run SingleUpperLimitTask --version 20230904_all_corrections_6classes --region tH+ttH --variable dnn_score_max --binning threshold --freeze-params r_tH --signal-poi r_ttH
law run PlotUpperLimitPointsTask --version 20230904_all_corrections_6classes --region tH+ttH --variable dnn_score_max --binning threshold --print-status -1
law run PlotUpperLimitPointsTask --version 20230904_all_corrections_6classes --region tH+ttH --variable dnn_score_max --binning threshold --freeze-params r_tH --signal-poi r_ttH
law run PlotUpperLimitPointsTask --version 20230829_all_corrections_4classes --category inclusive --variable dnn_score_max --binning equal_width --signal-poi r_tH --freeze-params r_ttH --label r_{tH} --process tH
law run PlotUpperLimitPointsTask --version 20230829_all_corrections_4classes --category inclusive --variable dnn_score_max --binning equal_width --signal-poi r_ttH --freeze-params r_tH --label r_{ttH} --process ttH
```

### kTop Sensitivity
```bash
law run PlotLikelihoodTask --version 20230904_all_corrections_6classes --binning threshold --variable dnn_score_max --points 350 --scan-range 10 --x-min -4 --x-max 4 --remove-output 0,a,1
```

### N-1
```bash
law run PlotNMinusOneTask --NMOversion 20230908_after_search_4classes --version 20230829_all_corrections_4classes --binning threshold --variable dnn_score_max --points 350 --scan-range 10 --workers 14
```

```bash
law run PlotNMinusOneTask --NMOversion 20230912_merged_steps_6classes --version 20230904_all_corrections_6classes --binning threshold --variable dnn_score_max --points 350 --scan-range 10 --markersize 0.6 --workers 14 --remove-output 0,a,1
https://cernbox.cern.ch/files/spaces/eos/user/v/vschwan/txHx/store/th_tth_tthh/Run2_pp_13TeV_2018/DatacardProducer/20230904_all_corrections_6classes/txhx/nlo/StatModel/20230908_after_search_6classes/PlotNMinusOneTask/dnn_score_max/threshold/range_10.0/350
```
