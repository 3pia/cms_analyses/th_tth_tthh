# coding: utf-8
# flake8: noqa

"""
the setup_categories is called in analysis.py
"""

import utils.aci as aci

from ..recipes.common import get_parameter_from_model_path


def setup_categories(cfg):
    getproc = cfg.processes.get
    multiclass_config = cfg.aux["multiclass"]
    dnn_model_path = cfg.aux["dnn_model"]
    multiclass_group = "_".join(
        get_parameter_from_model_path(dnn_model_path, "multiclass_group_").split("_")[2:-1]
    )
    multiclass_processes = multiclass_config.groups[multiclass_group]
    multiclasses = {
        f"dnn_node_{classname}": getproc(classname).label
        for classname in multiclass_processes.keys()
    }

    # for ch, chtex in {"e": "e", "mu": r"μ", "all": "all"}.items():
    for ch, chtex in {
        "tH": r"=3 btags, $\geq$ 4 jets",
        "ttH": r"$\geq$ 4 btags, $\geq$ 5 jets",
        # "ttH": r"4-5 btags, $\geq$ 5 jets",
        # "ttHH": r"$\geq$ 6 btags, $\geq$ 6 jets",
        "inclusive": r"$\geq$ 3 btags, $\geq$ 4 jets",
    }.items():
        cfg.categories.add(
            aci.Category(name=ch, label=chtex, aux=dict(fit_variable="bJets_MinvBB-leadingpt12"))
        )
        for dnn_class, dnn_class_label in multiclasses.items():
            cfg.categories.add(
                aci.Category(
                    name=f"{ch}_{dnn_class}",
                    label=f"{chtex} \N{MINUS SIGN} Node: {dnn_class_label}",
                    aux={"fit_variable": "dnn_score_max"},
                )
            )

    cfg.aux["collate_cats"] = {}
