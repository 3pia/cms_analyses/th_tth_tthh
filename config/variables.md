<!-- title: Variables -->

List of Variables as listed in AN2019_094_v15
============================================================

# DNN Variables

**only Semi-Leptonic Channel (SL)**

variables marked with a * are constructed using information from the BDT-based event reconstruction

|                                 |                                    Observable                                    | $\ge$ 6Jets / 4 b-tags | 5 jets, $\ge$ 4 b-tags | implemented |
| :-----------------------------: | :------------------------------------------------------------------------------: | :--------------------: | :--------------------: | :---------: |
|               MEM               |                        matrix element method discriminant                        |        &check;         |        &check;         |   &cross;   |
|    $\ln(\frac{BLR}{1-BLR})$     |               transformed b tagging likelihood ratio discriminant                |        &check;         |        &check;         |   &cross;   |
|    $\langle d_b(j) \rangle$     |                 average b tagging discriminant value of all jets                 |        &check;         |        &check;         |   &check;   |
|    $\langle d_b(b) \rangle$     |             average b taggin discriminant value of all b-tagged jets             |        &check;         |        &check;         |   &check;   |
|           $d^3_b(j)$            |             third highest b tagging discriminant values of all jets              |        &check;         |        &check;         |   &check;   |
|          $Var(d_b(j))$          |              variance of b tagging discriminant values of all jets               |        &check;         |        &check;         |   &check;   |
| $\langle \Delta\eta(bb)\rangle$ |                average of $\Delta\eta$ between two b-tagged jets                 |        &check;         |        &check;         |   &check;   |
| $\langle \Delta\eta(jj)\rangle$ |                     average of $\Delta\eta$ between two jets                     |        &check;         |        &check;         |   &check;   |
|     $\langle m(b) \rangle$      |                   average invariant mass of all b-tagged jets                    |        &check;         |        &check;         |   &check;   |
|     $\langle m(j) \rangle$      |                        average invariant mass of all jets                        |        &check;         |        &check;         |   &check;   |
|     $m(bb_{\min \Delta R})$     |          invariant mass of pair of b-tagged jets closest in $\Delta R$           |        &check;         |        &check;         |   &check;   |
|     $\langle p_T(j)\rangle$     |                            average $p_T$ of all jets                             |        &check;         |        &check;         |   &check;   |
|     $\langle p_T(b)\rangle$     |                        average $p_T$ of all b-tagged jets                        |        &check;         |        &check;         |   &check;   |
|    $p_T(bb_{\min \Delta R})$    |               scalar sum of $p_T$ of pair of closest b-tagged jets               |        &check;         |        &check;         |   &check;   |
|            $H_T(j)$             |                         scalar sum of $p_T$ of all jets                          |        &check;         |        &check;         |   &check;   |
|            $H_T(b)$             |                     scalar sum of $p_T$ of all b-tagged jets                     |        &check;         |        &cross;         |   &check;   |
|     $d_b(b^{tHW}_{top})$ *      | b-tagging discriminant value of b jet from t quark decay from tHW reconstruction |        &check;         |        &check;         |   &cross;   |
|   $\vert\eta(q^{tHq})\vert$ *   |           $\vert\eta\vert$ of light-quark jet from tHq reconstruction            |        &check;         |        &check;         |   &cross;   |
|      $m(t^{ttH}_{lep})$ *       | invariant mass of leptonically decaying t quark from $t\bar{t}H$ reconstruction  |        &check;         |        &check;         |   &cross;   |
|            $BDT^i$ *            |     reconstruction BDT output for $tHq$, $t\bar{t}H$, $t\bar{t}$ hypotheses      |        &check;         |        &check;         |   &cross;   |
