# coding: utf-8
# flake8: noqa

"""
Definition of the tH+ttH+ttHH analysis
"""

import pathlib
import utils.aci as aci
from config.analysis import analysis
from config.util import PrepareConfig
from tasks.multiclass import MulticlassConfig
from th_tth_tthh.config.categories import setup_categories
from th_tth_tthh.config.variables import setup_variables
from utils.aci import rgb

# create the analysis
analysis = analysis.copy(name="th_tth_tthh", label="tH + ttH + ttHH", label_short="txHx")


# define analysis specific processes and datasets
specific_processes = []
specific_datasets = []

process_names = ["tt+b", "tt+2b", "tt+bb", "tt+cc", "tt+lf"]
process_labels = [
    "$t\\bar{t}+b$",
    "$t\\bar{t}+2b$",
    "$t\\bar{t}+bb$",
    "$t\\bar{t}+cc$",
    "$t\\bar{t}+lf$",
]

tt_xsec = {13: analysis.processes.get("tt").xsecs[13]}
# name, id5fs, id4fs
tt_subdatasets = [("Hadronic", 2100, 2600), ("SemiLeptonic", 2200, 2700), ("2L2Nu", 2300, 2800)]
xsecs = {
    "Hadronic": {13: analysis.processes.get("tt_fh").xsecs[13]},
    "SemiLeptonic": {13: analysis.processes.get("tt_sl").xsecs[13]},
    "2L2Nu": {13: analysis.processes.get("tt_dl").xsecs[13]},
}

# for process_name, process_label in zip(process_names, process_labels):
#     for subset in tt_subdatasets:
#         pn = f"{process_name}_{subset}"
#         process = aci.Process(
#             name=pn,
#             is_data=False,
#             label=f"{process_label}_{subset}",
#             aux=dict(dynamic=True),
#             xsecs=xsecs[subset],
#         )
#         dataset = aci.Dataset(name=pn, is_data=False)
#         dataset.processes.add(process)
#         specific_processes.append(process)
#         specific_datasets.append(dataset)

dataset_id = 299
process_id = 1
for process_name, process_label in zip(process_names, process_labels):
    subprocesses = []
    for subset, id5fs, id4fs in tt_subdatasets:
        subprocess_name = f"{process_name}_{subset}"
        subprocess_label = f"{process_label} ({subset})"
        subprocess = aci.Process(
            name=subprocess_name,
            is_data=False,
            label=subprocess_label,
            aux=dict(dynamic=True),
            xsecs=xsecs[subset],
            id=(id4fs if "b" in process_name else id5fs) + process_id,
        )
        subprocesses.append(subprocess)

        subdataset = aci.Dataset(name=subprocess_name, is_data=False, id=dataset_id)
        subdataset.processes.add(subprocess)
        specific_datasets.append(subdataset)
        dataset_id -= 1

    process = aci.Process(
        name=process_name,
        is_data=False,
        label=process_label,
        aux=dict(dynamic=True),
        xsecs=tt_xsec,
        processes=subprocesses,
        id=(2000 + 10 * process_id),
    )
    specific_processes.append(process)
    process_id += 1

for process_name in ["tt+bb", "tt+b", "tt+2b"]:
    subprocesses = []
    for subset, id5fs, id4fs in tt_subdatasets:
        subprocess_name = f"{process_name}_{subset}_5fs"
        subprocess_label = f"{process_label} ({subset})_5fs"
        subprocess = aci.Process(
            name=subprocess_name,
            is_data=False,
            label=subprocess_label,
            aux=dict(dynamic=True),
            xsecs=xsecs[subset],
            id=(id4fs if "b" in process_name else id5fs) + process_id,
        )
        subprocesses.append(subprocess)

        subdataset = aci.Dataset(name=f"{subprocess_name}", is_data=False, id=dataset_id)
        subdataset.processes.add(subprocess)
        specific_datasets.append(subdataset)
        dataset_id -= 1

    process = aci.Process(
        name=f"{process_name}_5fs",
        is_data=False,
        label=f"{process_name}_5fs",
        aux=dict(dynamic=True),
        xsecs=tt_xsec,
        processes=subprocesses,
        id=(2000 + 10 * process_id),
    )
    specific_processes.append(process)
    process_id += 1

# add analysis specific processes
analysis.processes.extend(specific_processes)

process_ttB = aci.Process(
    name="tt+B",
    label="$t\\bar{t}+B$",
    is_data=False,
    aux=dict(dynamic=True),
    xsecs=tt_xsec,
    processes=[analysis.processes.get(p) for p in ["tt+bb", "tt+b", "tt+2b"]],
    id=(2000 + 10 * process_id),
)

analysis.processes.extend([process_ttB])
# add datasets after PrepareConfig to prevent them from being thrown out directly

process_ttB_5fs = aci.Process(
    name="tt+B_5fs",
    label="$t\\bar{t}+B$",
    is_data=False,
    aux=dict(dynamic=True),
    xsecs=tt_xsec,
    processes=[analysis.processes.get(p) for p in ["tt+bb_5fs", "tt+b_5fs", "tt+2b_5fs"]],
    id=(2000 + 10 * process_id + 1),
)

analysis.processes.extend([process_ttB_5fs])


plotting_procs = [
    "data",
    # "THQ_ctcvcp_4f_Hincl",
    # "THW_ctcvcp_5f_Hincl",
    "TH",
    "ttH",
    # "tt",
    # "tt+b",
    # "tt+2b",
    # "tt+bb",
    "tt+B",
    "tt+lf",
    "tt+cc",
    "ttV",
    "st",
    "vv",
    "wjets",
    "dy",
]
only5fs_procs = [
    "data",
    "TH",
    "ttH",
    "tt",
    "ttV",
    "st",
    "vv",
    "wjets",
    "dy",
]
unblinded_procs = [
    "data",
    # "tt",
    # "tt+b",
    # "tt+2b",
    # "tt+bb",
    "tt+B",
    "tt+lf",
    "tt+cc",
    "ttV",
    "st",
    "vv",
    "wjets",
    "dy",
]


def get_proc(name: str):
    assert name in analysis.processes.keys, f"{name} not defined in analysis.processes"
    return analysis.processes.get(name)


def create_processgroup(name: str, children: tuple, **kwargs) -> str:
    group = analysis.processes.new(name=name, processes=list(map(get_proc, children)), **kwargs)
    signal = get_proc("H")
    if any(map(signal.has_process, group.processes)):
        signal.processes.add(group)
    return group.name


analysis.aux["process_groups"] = {
    "plotting": plotting_procs,
    "unblinded": unblinded_procs,
    "only5fs": only5fs_procs,
    # the following ones are for plotting purposes, with enhanced color scheme (for color blindness)
    "six_classes": [
        # fmt: off
        create_processgroup("ttH_six_classes", ["ttH"], color=rgb(200, 37, 6), label=get_proc("ttH").label),
        create_processgroup("tH_six_classes", ["TH"], color=rgb(6, 37, 200), label=get_proc("TH").label),
        create_processgroup("tt+B_six_classes", ["tt+B"], color=rgb(193, 117, 166), label=get_proc("tt+B").label),
        create_processgroup("tt+lf_six_classes", ["tt+lf"], color=rgb(161, 193, 129), label=get_proc("tt+lf").label),
        create_processgroup("tt+cc_six_classes", ["tt+cc"], color=rgb(168, 218, 220), label=get_proc("tt+cc").label),
        create_processgroup("others_six_classes", ["ttV", "st", "vv", "wjets", "dy"], color=rgb(249, 216, 117), label="Other"),
        # fmt: on
        "data",
    ],
    "four_classes": [
        # fmt: off
        create_processgroup("ttH_four_classes", ["ttH"], color=rgb(200, 37, 6), label=get_proc("ttH").label),
        create_processgroup("tH_four_classes", ["TH"], color=rgb(6, 37, 200), label=get_proc("TH").label),
        create_processgroup("tt+jets_four_classes", ["tt+B","tt+lf","tt+cc"], color=rgb(193, 117, 166), label="tt+jets"),
        create_processgroup("others_four_classes", ["ttV", "st", "vv", "wjets", "dy"], color=rgb(249, 216, 117), label="Other"),
        # fmt: on
    ],
    "six_classes_unblind": [
        # fmt: off
        #create_processgroup("ttH_compact_blind", ["ttH"], color=rgb(200, 37, 6), label=get_proc("ttH").label),
        #create_processgroup("tH_compact_blind", ["TH"], color=rgb(6, 37, 200), label=get_proc("TH").label),
        create_processgroup("tt+B_compact_blind", ["tt+B"], color=rgb(193, 117, 166), label=get_proc("tt+B").label),
        create_processgroup("tt+lf_compact_blind", ["tt+lf"], color=rgb(161, 193, 129), label=get_proc("tt+lf").label),
        create_processgroup("tt+cc_compact_blind", ["tt+cc"], color=rgb(168, 218, 220), label=get_proc("tt+cc").label),
        create_processgroup("others_compact_blind", ["ttV", "st", "vv", "wjets", "dy"], color=rgb(249, 216, 117), label="Other"),
        # fmt: on
        "data",
    ],
}

analysis.aux["btag_sf_shifts"] = [
    "lf",
    "lfstats1",
    "lfstats2",
    "hf",
    "hfstats1",
    "hfstats2",
    "cferr1",
    "cferr2",
]

PrepareConfig(
    analysis,
    # defines which datasets we will use
    # These are SL datasets only
    processes=[
        "data_sl",
        # "THQ_ctcvcp_4f_Hincl",
        # "THW_ctcvcp_5f_Hincl",
        "TTHH",
        "TH",
        "ttH",
        "tt",
        "TTbb_4f",  # 4f sample for ttbb background
        "ttV",
        "st",
        "vv",
        "dy_lep_nj",  # TODO: prefer HT binned
        "dy_lep_10To50",
        "WJetsToLNu_HT",
    ],
    # allowed_exclusive_processes=[
    #     "wjets_lep",
    #     "dy_50to",
    # ],
)

# create parent process others, so we can use it as DNN Class
others = ["ttV", "st", "vv", "wjets", "dy"]

process_others = aci.Process(
    name="others",
    label="others",
    is_data=False,
    aux=dict(dynamic=True),
    processes=[analysis.processes.get(p) for p in others],
    id=314159,
)

process_tt_4FS5FSmerged = aci.Process(
    name="tt_4FS5FSmerged",
    label="tt+jets",
    label_short="tt+jets",
    is_data=False,
    aux=dict(dynamic=True),
    processes=[analysis.processes.get(p) for p in ["tt+B", "tt+cc", "tt+lf"]],
    id=236947,
)

analysis.processes.extend([process_others, process_tt_4FS5FSmerged])


analysis.aux["multiclass"] = MulticlassConfig(
    groups={
        "base": {
            "TH": {"groups": ["signal", "fit", "norm"]},
            "ttH": {"groups": ["signal", "fit", "norm"]},
            # "TTHH": {"groups": ["signal", "fit", "norm"]},
            "tt+B": {"groups": ["background"]},
            "tt+lf": {"groups": ["background"]},
            "tt+cc": {"groups": ["background"]},
            "ttV": {"groups": ["background"]},
            "st": {"groups": ["background"]},
            "vv": {"groups": ["background"]},
            "wjets": {"groups": ["background"]},
            "dy": {"groups": ["background"]},
        },
        "tt+j_splitted": {
            "TH": {"groups": ["signal", "fit", "norm"]},
            "ttH": {"groups": ["signal", "fit", "norm"]},
            # "TTHH": {"groups": ["signal", "fit", "norm"]},
            "tt+B": {"groups": ["background"]},
            "tt+lf": {"groups": ["background"]},
            "tt+cc": {"groups": ["background"]},
            "others": {"groups": ["background"]},
        },
        "default": {
            "TH": {"groups": ["signal", "fit", "norm"]},
            "ttH": {"groups": ["signal", "fit", "norm"]},
            # "TTHH": {"groups": ["signal", "fit", "norm"]},
            "tt_4FS5FSmerged": {"groups": ["background"]},
            "others": {"groups": ["background"]},
        },
    },
    group="default",
    maxn=int(2e6),
)


# add analysis specific datasets
for acv in analysis.campaigns.values:
    acv.datasets.extend(specific_datasets)


analysis.processes.get("ttH").aux["style"] = dict(linestyle="--", color=(0.2, 0.15, 0.65))
analysis.processes.get("TH").aux["style"] = dict(linestyle="--", color=(0.51, 0.17, 0.18))
analysis.processes.get("TTHH").aux["style"] = dict(
    linestyle="--", color=(0, 102.0 / 255.0, 51.0 / 255.0)
)

# === DNN Model Path ===
# needs to be defined before setup of categories/variables
analysis.aux[  # fmt: off
    "dnn_model"
] = "/net/scratch/cms/dihiggs/store/th_tth_tthh/MulticlassOptimizedStitched/20230825_new_base_with_bTagNorm/multiclass_group_tt+j_splitted_6/years_2018/stitch_version_20230904_6classes_2nd_search/multiclass_splits_5/model"  # "/net/scratch/cms/dihiggs/store/th_tth_tthh/MulticlassOptimizedStitched/20230825_new_base_with_bTagNorm/multiclass_group_default_4/years_2018/stitch_version_20230825_stitched/multiclass_splits_5/model"
# fmt: on

setup_categories(analysis)
setup_variables(analysis)

#
# other configurations
#

analysis.aux["signal_process"] = "H"

# RegExp for Vispa Pscan
analysis.aux[
    "regex"
] = r"(?P<binning>.+)/(?P<Object>.+)_(?P<variable>.+)/(?P<category>.+)/(?P<blind_thresh>.+)_(?P<scale>.+)\.(?P<format>[^\.]+)"


# === create table with datasets ===
for campaign in analysis.campaigns:
    name = campaign.name
    table = campaign.datasets.print_datasets(print_to_console=False, tablefmt="latex")
    path = pathlib.Path(__file__).parent.absolute()
    with open(path.joinpath(f"{name}_datasets.tex"), "w") as file:
        file.write(table)
