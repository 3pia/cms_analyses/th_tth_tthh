# coding: utf-8
# flake8: noqa

"""
explanation of tags:
====================

fit: ? do we need this anymore # TODO

shifts:
    if this key is in tags, all shifts are computed and saved in the systematics axis of the histogram
    Note: you need variable_full_shifts function for that
    check in recipes.common Base class
dnn_score: ? do we need this, probably not # TODO

rebin:
    if rebin in tags, the histogram will be rebinned in StatModle rebin function
    else the bins specified at time of variable creation are kept

not_regularize:
    in task.plotting  a histogram with unequal binwidths will be regularized
    histogram bins are converted to integer bins with equal width
    if you want to keep unequal bins, set this tag

binwnorm:
    in case of unequal bins you might want to normalize the bins to their width
    if tag binwnorm will be set to 1.0
    for more information check
    https://mplhep.readthedocs.io/en/latest/api.html?highlight=histplot#mplhep.histplot

"""


# from math import pi

import utils.aci as aci


def setup_variables(cfg):
    # # === number of primary vertices ===
    # cfg.variables.add(
    #     aci.Variable(
    #         name="PV_N",
    #         expression="number_pv",
    #         binning=(80, 0.0, 80.0),
    #         x_title=r"Number Primary Vertices (npvsGood)",
    #     )
    # )

    # # === met ===
    # cfg.variables.add(
    #     aci.Variable(
    #         name="MET_pt",
    #         expression="met.pt",
    #         binning=(40, 0.0, 200.0),
    #         unit="GeV",
    #         x_title=r"$p_{T}^{miss}$",
    #     )
    # )
    # cfg.variables.add(
    #     aci.Variable(
    #         name="MET_phi",
    #         expression="met.phi",
    #         binning=(30, -3.3, 3.3),
    #         x_title=r"MET $\phi$",
    #     )
    # )

    # # === HT ===
    # cfg.variables.add(
    #     aci.Variable(
    #         name="Jets_HT",
    #         expression="ht",
    #         binning=(200, 0.0, 1000.0),
    #         unit="GeV",
    #         x_title=r"$H_T$ (not cleaned)",
    #     )
    # )

    # === Jets ===
    # we need variable with name njets to perform btagnorm correction
    cfg.variables.add(
        aci.Variable(
            name="njets",
            expression="ak.num(clean_good_jets)",
            binning=(10, 2.5, 12.5),
            x_title="Number of Jets",
        )
    )

    cfg.variables.add(
        aci.Variable(
            name="Jets_N",
            expression="ak.num(clean_good_jets)",
            binning=(10, 2.5, 12.5),
            x_title=r"Number of Jets",
        )
    )
    # cfg.variables.add(
    #     aci.Variable(
    #         name="Jets_NBTagsM",
    #         expression="n_btag",
    #         binning=(9, 1.5, 10.5),
    #         x_title=r"Number of BTag (medium)",
    #     )
    # )
    # cfg.variables.add(
    #     aci.Variable(
    #         name="Jets_BTagsM",
    #         expression="clean_good_jets.btagDeepFlavB",
    #         binning=(30, 0.0, 1.0),
    #         x_title=r"Jets btagDeepFlavB",
    #     )
    # )
    # cfg.variables.add(
    #     aci.Variable(
    #         name=f"Jets_pt",
    #         expression=f"clean_good_jets.pt",
    #         binning=(40, 0.0, 400.0),
    #         x_title=rf"Jets $p_T$",
    #     )
    # )
    cfg.variables.add(
        aci.Variable(
            name=f"Jets_eta",
            expression=f"clean_good_jets.eta",
            binning=(25, -2.5, 2.5),
            x_title=rf"Jets $\eta$",
        )
    )

    # for jetnr in range(4):
    #     cfg.variables.add(
    #         aci.Variable(
    #             name=f"Jet{jetnr + 1}_pt",
    #             expression=f"util.normalize(clean_good_jets.pt, pad=4, clip=True)[:, {jetnr}]",
    #             unit="GeV",
    #             binning=(40, 0.0, 400.0),
    #             x_title=rf"Jet[{jetnr}] $p_T$",
    #         )
    #     )
    #     cfg.variables.add(
    #         aci.Variable(
    #             name=f"Jet{jetnr + 1}_eta",
    #             expression=f"util.normalize(clean_good_jets.eta, pad=4, clip=True)[:, {jetnr}]",
    #             binning=(50, -2.5, 2.5),
    #             x_title=rf"Jet[{jetnr}] $\eta$",
    #         )
    #     )
    #     cfg.variables.add(
    #         aci.Variable(
    #             name=f"Jet{jetnr+1}_BTag-leading-pt",
    #             expression=f"util.normalize(clean_good_jets.btagDeepFlavB, pad=4, clip=True)[:, {jetnr}]",
    #             binning=(30, 0.0, 1.0),
    #             x_title=rf"Jet[{jetnr}] btagDeepFlavB: leading pt",
    #         )
    #     )
    #     cfg.variables.add(
    #         aci.Variable(
    #             name=f"Jet{jetnr+1}_BTag-leading-score",
    #             expression=f"util.normalize(ak.sort(clean_good_jets.btagDeepFlavB, axis=1, ascending=False), pad=4, clip=True)[:, {jetnr}]",
    #             binning=(30, 0.0, 1.0),
    #             x_title=rf"Jet[{jetnr}] btagDeepFlavB: leading score",
    #         )
    #     )

    # === DNN Variables ===

    DNN_VARIABLES = [
        (
            "Jets_averageBTag",
            "ak.mean(clean_good_jets_btag, axis=-1)",
            (50, 0.0, 1.0),
            r"$\langle d_b(j)\rangle$",
        ),
        (
            "Jets_varianceBtag",
            "ak.var(clean_good_jets_btag, axis=-1)",
            (20, 0.0, 0.4),
            "Var$(d_b(j))$",
        ),
        (
            "bJets_averageBtag",
            "ak.mean(clean_good_b_jets_btag, axis=-1)",
            (400, 0.0, 1.0),
            r"$\langle d_b(b)\rangle$",
            {
                "tags": {"shifts", "not_regularize"},  # , "rebin"
                "aux": {"rebin_nbins": 20},
            },
        ),
        (
            "Jets_thirdHighestBtag",
            "third_highest_btag_all_jets",
            (50, 0.0, 1.0),
            "$d^3_b(j)$",
            {"tags": {"shifts", "rebin"}},
        ),
        (
            "bJets_averageDeltaEta",
            "average_delta_eta_b_jets",
            (25, 0.0, 2.5),
            r"$\langle \Delta\eta(bb) \rangle$",
            {"tags": {"shifts", "rebin"}},
        ),
        (
            "Jets_averageDeltaEta",
            "average_delta_eta_jets",
            (25, 0.0, 2.5),
            r"$\langle \Delta\eta(jj) \rangle$",
        ),
        (
            "bJets_averageMinv",
            "average_minv_all_b_jets",
            (40, 0.0, 80.0),
            r"$\langle m(b) \rangle$",
            {"unit": "GeV"},
        ),
        (
            "Jets_averageMinv",
            "average_minv_all_jets",
            (40, 0.0, 80.0),
            r"$\langle m(j) \rangle$",
            {"unit": "GeV"},
        ),
        (
            "bJets_Minv-ClosestDR",
            "minv_bjets_closest_dR",
            (400, 0.0, 400.0),
            "$m(bb_{\min \Delta R})$",
            {
                "tags": {"shifts", "rebin", "not_regularize"},
                "unit": "GeV",
                "aux": {"rebin_nbins": 80},
            },
        ),
        (
            "bJets_Minv-MaximumDR",
            "minv_bjets_maximum_dR",
            (400, 0.0, 400.0),
            "$m(bb_{\max \Delta R})$",
            {
                "tags": {"shifts", "rebin", "not_regularize"},
                "unit": "GeV",
                "aux": {"rebin_nbins": 80},
            },
        ),
        (
            "Jets_averagePT",
            "average_pt_all_jets",
            (60, 0.0, 300.0),
            r"$\langle p_T(j) \rangle$",
            {"unit": "GeV"},
        ),
        (
            "bJets_averagePT",
            "average_pt_all_b_jets",
            (60, 0.0, 300.0),
            r"$\langle p_T(b) \rangle$",
            {"unit": "GeV", "tags": {"shifts", "rebin"}},
        ),
        (
            "bJets_sumPT-Closest",
            "sum_pt_closest_b_jets",
            (40, 0.0, 400.0),
            "$p_T(bjets_{\min\Delta R})$",
            {"unit": "GeV", "tags": {"shifts", "rebin"}},
        ),
        (
            "Jets_sumPT",
            "sum_pt_all_jets",
            (50, 0.0, 1000.0),
            "$H_T(j)$",
            {"unit": "GeV", "tags": {"shifts", "rebin"}},
        ),
        ("bJets_sumPT", "sum_pt_all_b_jets", (50, 0.0, 1000.0), "$H_T(b)$", {"unit": "GeV"}),
        (
            "bJets_MinvBB-leadingbtag12",
            "minv_bb_leading_btag_12",
            (400, 0.0, 400.0),
            "$m_{inv}(bb)$ (first, second leading btag)",
            {
                "tags": {"shifts", "rebin", "not_regularize"},
                "unit": "GeV",
                "aux": {"rebin_nbins": 80},
            },
        ),
        (
            "bJets_MinvBB-leadingbtag23",
            "minv_bb_leading_btag_23",
            (400, 0.0, 400.0),
            "$m_{inv}(bb)$ (second, third leading btag)",
            {
                "tags": {"shifts", "rebin", "not_regularize"},
                "unit": "GeV",
                "aux": {"rebin_nbins": 80},
            },
        ),
        (
            "bJets_MinvBB-leadingpt12",
            "minv_bb_leading_pt_12",
            (400, 0.0, 400.0),
            "$m_{inv}(bb)$ (first, second leading pt)",
            {
                "tags": {"shifts", "rebin", "not_regularize"},
                "unit": "GeV",
                "aux": {"rebin_nbins": 80},
            },
        ),
        (
            "bJets_MinvBB-leadingpt23",
            "minv_bb_leading_pt_23",
            (400, 0.0, 400.0),
            "$m_{inv}(bb)$ (second, third leading pt)",
            {
                "tags": {"shifts", "rebin", "not_regularize"},
                "unit": "GeV",
                "aux": {"rebin_nbins": 80},
            },
        ),
        (
            "bJets_eta-leadingbtag12",
            "eta_bb_leading_btag_12",
            (500, -2.5, 2.5),
            "$\eta(bb)$ (first, second leading btag)",
            # {"tags": {"shifts", "rebin", "not_regularize"}},
        ),
    ]

    # explanation
    # name, expression, binning, x_title have to be in the tuple above
    # additional arguments have to be passed with a dictionary
    # i.e. (name, expression, binning, x_title, {"tags": {set of tags}}, {"unit": "GeV"})

    for name, expression, binning, x_title, *args in DNN_VARIABLES:
        kwargs = {key: item[key] for item in args for key in item}
        cfg.variables.add(
            aci.Variable(
                name=name, expression=expression, binning=binning, x_title=x_title, **kwargs
            )
        )

    # single histogram with max dnn prediction output
    cfg.variables.add(
        aci.Variable(
            name="dnn_score_max",
            expression="np.max(pred, axis=-1)",
            binning=(400, 0.0, 1.0),
            unit="",
            x_title=r"DNN score",
            tags={"fit", "shifts", "dnn_score", "rebin", "not_regularize"},
            aux={"rebin_nbins": 20},
        )
    )

    # === leptons ===

    # cfg.variables.add(
    #     aci.Variable(
    #         name=f"Lep_pt",
    #         expression=f"util.normalize(good_leptons.pt, pad=1, clip=True)[:,0]",
    #         binning=(50, 0.0, 300.0),
    #         unit="GeV",
    #         x_title=rf"Lepton $p_T$",
    #     )
    # )
    # cfg.variables.add(
    #     aci.Variable(
    #         name=f"Lep_eta",
    #         expression=f"util.normalize(good_leptons.eta, pad=1, clip=True)[:,0]",
    #         binning=(50, -2.5, 2.5),
    #         x_title=rf"Lepton $\eta$",
    #     )
    # )
    # cfg.variables.add(
    #     aci.Variable(
    #         name=f"e_pt",
    #         expression=f"util.normalize(good_electrons.pt, pad=1, clip=True)[:,0]",
    #         binning=(50, 0.0, 400.0),
    #         unit="GeV",
    #         x_title=rf"Electron $p_T$",
    #     )
    # )
    # cfg.variables.add(
    #     aci.Variable(
    #         name=f"e_eta",
    #         expression=f"util.normalize(good_electrons.eta, pad=1, clip=True)[:,0]",
    #         binning=(50, -2.5, 2.5),
    #         x_title=rf"Electron $\eta$",
    #     )
    # )
    # cfg.variables.add(
    #     aci.Variable(
    #         name=f"mu_pt",
    #         expression=f"util.normalize(good_muons.pt, pad=1, clip=True)[:,0]",
    #         binning=(50, 0.0, 300.0),
    #         unit="GeV",
    #         x_title=rf"Muons $p_T$",
    #     )
    # )
    # cfg.variables.add(
    #     aci.Variable(
    #         name=f"mu_eta",
    #         expression=f"util.normalize(good_muons.eta, pad=1, clip=True)[:,0]",
    #         binning=(50, -2.5, 2.5),
    #         x_title=rf"Muons $\eta$",
    #     )
    # )

    # === weights ===
    cfg.variables.add(
        aci.Variable(
            name="weight_total",
            binning=(100, 0.0, 0.2),  # optimal for GGF sync tuple
            unit="1",
            x_title=r"$w_{tot}$",
            aux={"histogram": False},
        )
    )
    cfg.variables.add(
        aci.Variable(
            name="weight_gen",
            binning=(100, 0.0, 0.2),  # optimal for GGF sync tuple
            unit="1",
            x_title=r"$w_{gen}$",
            aux={"histogram": False},
        )
    )
