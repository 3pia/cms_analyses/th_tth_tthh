# Detailed documentation on the use of the multiclass code
- previously in README

## 07.09.2023
```bash
law run PlotsCards --version 20230904_all_corrections_6classes --year 2018 --remove-output 3,a,1
law run PlotNMinusOneTask --NMOversion 20230907_test --version 20230829_all_corrections_4classes --binning equal_frequency_sig --variable dnn_score_max --points 10 --scan-range 10 --campaign 2018_short --remove-output 0,a,1
```

## 06.09.2023
```bash
law run CompareLikelihoodsTask --version 20230829_all_corrections_4classes:20230904_all_corrections_6classes --variable dnn_score_max --binning equal_frequency_sig:equal_frequency_bkg:equal_width:threshold --category tH_dnn_node_TH:tH_dnn_node_ttH:tH_dnn_node_tt_4FS5FSmerged:tH_dnn_node_others:ttH_dnn_node_TH:ttH_dnn_node_ttH:ttH_dnn_node_tt_4FS5FSmerged:ttH_dnn_node_others

law run CompareLikelihoodsTask --version 20230829_all_corrections_4classes --variable dnn_score_max --binning equal_frequency_sig:equal_frequency_bkg:equal_width:threshold --category tH_dnn_node_TH:tH_dnn_node_ttH:tH_dnn_node_tt_4FS5FSmerged:tH_dnn_node_others:ttH_dnn_node_TH:ttH_dnn_node_ttH:ttH_dnn_node_tt_4FS5FSmerged:ttH_dnn_node_others
```

## 05.09.2023
```bash
law run PlotNMinusOneTask --version 20230829_all_corrections_4classes --binning equal_width --variable dnn_score_max --category tH:ttH --points 30 --scan-range 10 --campaign 2018_short --remove-output 1,a,1
```

## random access 02.09.2023
```bash
law run CoffeaProcessor --version 20230902_6classes_all_corrections --recipe txhx --year 2018
law run MulticlassOptStitchedEvaluation --version 20230825_new_base_with_bTagNorm --stitch-version 20230901_6classes_1st_search --recipe txhx --group-override tt+j_splitted
law run MulticlassOptimizerPlot --version 20230825_new_base_with_bTagNorm --opt-version 20230831_6classes_2nd_search --group-override tt+j_splitted --recipe txhx --n-initial 50 --n-parallel 50 --iterations 250 --workers 50 --remove-output 0,i,1
law run MulticlassOptimizerPlot --version 20230825_new_base_with_bTagNorm --opt-version 20230828_6classes_1st_search --group-override tt+j_splitted --recipe txhx --n-initial 200 --n-parallel 100 --iterations 500 --workers 100 --remove-output 0,i,1
```

## random access 25.08.2023
```bash
law run CoffeaProcessor --version 20230825_stitched_new_trainings_all_corrections --recipe txhx --analysis-choice th_tth_tthh --year 2018
source setup.sh NAME
law run PlotLikelihoodTask --version 20230823_stitched_1st_3rd_fix_empty_hists --category ttH --binning equal_frequency_sig --variable dnn_score_max --LikelihoodScanTask-points 200 --campaign 2018_short --remove-output 1,a,1
law run DatacardProducer --version 20230823_stitched_1st_3rd_fix_empty_hists --recipe txhx --category ttH --variable dnn_score_max --year 2018 --binning-strategy equal_frequency_sig --transfer
law run CoffeaProcessor --version 20230825_new_base_with_bTagNorm --recipe txhx --year 2018
law run MulticlassOptimizedStitched --version 20230825_new_base_with_bTagNorm --stitch-version 20230825_stitched --analysis-choice th_tth_tthh --recipe txhx --workers 5 --remove-output 0,a,1
```

## random access 24.08.2023
```bash
./plots_4_strats.sh 20230823_stitched_1st_3rd_fix_empty_hists

law run PlotProducer --version 20230823_stitched_1st_3rd_fix_empty_hists --recipe txhx --year 2018 --log-scale --binning-strategy equal_frequency_sig --n-parallel 20 --remove-output 0,a,1
```

## random access 21.08.2023
```bash
law run MulticlassOptimizedStitched --version 20230710_lbn_prep --stitch-version 20230821_1st_campaign_3rd_new --analysis-choice th_tth_tthh --recipe txhx --workers 5 --remove-output 0,a,1

--version 20230817_stitched_2nd_search_1st_campaign
law run CoffeaProcessor --version 20230821_stitched_3rd_search_1st_campaign --recipe txhx --analysis-choice th_tth_tthh --year 2018
law run PlotProducer --version 20230817_stitched_2nd_search_1st_campaign --process-group unblinded --recipe txhx --year 2018 --blind-thresh 10000 --log-scale
```

## latest versions

### Array Exporter
```bash
--version 20230825_new_base_with_bTagNorm
```
- previous
```bash
--version 20230710_lbn_prep
```

### Processor with DNN model
- latest:
```bash
--version 20230829_all_corrections_4classes
```
- current:
```bash
--version 20230823_stitched_1st_3rd_fix_empty_hists
```

### DNN score plots
```bash
--version 20230728_DNN_hists
```
- path:
```bash
/net/scratch/cms/dihiggs/store/th_tth_tthh/Run2_pp_13TeV_2018/PlotProducer/20230728_DNN_hists/txhx/0fbde6c3f805c37398a6b63411791e1e09cd6684ce5a418ed7a1818f2e8481e8/nlo/StatModel/equal_width/dnn_score_max/
```

### MulticlassEval 4-class scheme (tt+jets merged)

#### Stitched Models
- current:

version:
```bash
--version 20230825_new_base_with_bTagNorm --stitch-version 20230825_stitched
```
model:
```bash
/net/scratch/cms/dihiggs/store/th_tth_tthh/MulticlassOptimizedStitched/20230825_new_base_with_bTagNorm/multiclass_group_default_4/years_2018/stitch_version_20230825_stitched/multiclass_splits_5/model
```
plots:
```bash
/net/scratch/cms/dihiggs/store/th_tth_tthh/MulticlassOptStitchedEvaluation/20230825_new_base_with_bTagNorm/export_version_0/default/cut_neg/training_version_0/lbn_particles_0/FullyConnected/layers_4/nodes_380/activation_softplus/batch_norm_1/dropout_0.18/l2_1e-10/lossdef_default/optimizer_adam/learning_rate_1.60e-04/multiclass_group_default_4/years_2018/model_default/multiclass_splits_-1/stitch_version_20230825_stitched/plots
```

- previous:
  
path:
```bash
/net/scratch/cms/dihiggs/store/th_tth_tthh/MulticlassOptimizedStitched/20230710_lbn_prep/multiclass_group_default_4/years_2018/stitch_version_20230821_1st_campaign_3rd_new/multiclass_splits_5/model
```

#### Hyperparam searches

##### First search campaign 04.08.

###### First search
- version:
```bash
--version 20230710_lbn_prep --opt-version 0
```
- path:
```bash
/net/scratch/cms/dihiggs/store/th_tth_tthh/MulticlassOptimizerPlot/20230710_lbn_prep/opt_version_0/
```

###### Second search
- version:
```bash
--version 20230710_lbn_prep --opt-version 20230803_second_search
```

###### Third search
- version:
```bash
--version 20230710_lbn_prep --opt-version 20230804_third_search
```

###### Training after second search
- version:
```bash
law run MulticlassEvaluation --version 20230710_lbn_prep --analysis-choice th_tth_tthh --recipe txhx --year 2018 --training-version 20230804_best_params_second_search --group-override default --MulticlassTraining-training-steps-per-epoch 5000
```

- path:
```bash
/net/scratch/cms/dihiggs/store/th_tth_tthh/MulticlassEvaluation/20230710_lbn_prep/export_version_0/default/cut_neg/training_version_20230804_best_params_second_search/lbn_particles_0/FullyConnected/layers_4/nodes_380/activation_softplus/batch_norm_1/dropout_0.18/l2_1e-10/lossdef_default/optimizer_adam/learning_rate_1.60e-04/multiclass_group_default_4/years_2018/model_default/cw_/multiclass_splits_0/plots
```

##### Second search campaign 11.08.

###### First search
- version:
```bash
--version 20230710_lbn_prep --opt-version 20230811_1st_search DAY BEFORE IS BROKEN
```
- path:
```bash
/net/scratch/cms/dihiggs/store/th_tth_tthh/MulticlassOptimizerPlot/20230710_lbn_prep/opt_version_20230810_1st_search/
```

##### miscellaneous

- some old path:
```bash
/net/scratch/cms/dihiggs/store/th_tth_tthh/MulticlassEvaluation/20230604_DNNVariables_JesDatasetShifts/export_version_0/default/cut_neg/training_version_schwan_default_1/lbn_particles_0/FullyConnected/layers_3/nodes_256/activation_relu/batch_norm_1/dropout_0.0/l2_1e-08/lossdef_default/optimizer_adam/learning_rate_1.00e-03/multiclass_group_default_4/years_2018/model_default/cw_/0/plots
```

### MulticlassEval 6-class scheme (tt+jets splitted)
- path:
```bash
/net/scratch/cms/dihiggs/store/th_tth_tthh/MulticlassEvaluation/20230604_DNNVariables_JesDatasetShifts/export_version_0/default/cut_neg/training_version_schwan_splitted_1/lbn_particles_0/FullyConnected/layers_3/nodes_256/activation_relu/batch_norm_1/dropout_0.0/l2_1e-08/lossdef_default/optimizer_adam/learning_rate_1.00e-03/multiclass_group_tt+j_splitted_6/years_2018/model_default/cw_/0/plots
```

## commands

### ArrayExporter (flavor of CoffeaProcessor)
- used to export data after selection so that it can be used for Machine Learning applications
```bash
law run CoffeaProcessor --version 20230710_lbn_prep --recipe txhx --processor Exporter --analysis-choice th_tth_tthh --year 2018
```

### MulticlassEvaluation (and Training before)
- is an evaluation task for the trained multiclass classification model
- evaluates the model on the validation data, plots evaluation metrics, and saves the evaluation results and model summary

#### 4-class scheme (tt+jets merged)
```bash
law run MulticlassEvaluation --version 20230710_lbn_prep --recipe txhx --year 2018 --training-version long_base_test_1 --group-override default --MulticlassTraining-training-steps-per-epoch 5000 --MulticlassTraining-learning-rate 1e-2
```

#### 6-class scheme (tt+jets splitted)
```bash
law run MulticlassEvaluation --version 20230710_lbn_prep --recipe txhx --year 2018 --training-version long_base_test_1 --group-override tt+j_splitted --MulticlassTraining-training-steps-per-epoch 5000 --MulticlassTraining-learning-rate 1e-2
```

#### CheckGenerator
bug: does not work for six classes
```bash
law run CheckGenerator --version 20230825_new_base_with_bTagNorm --recipe txhx --year 2018 --group-override tt+j_splitted 
```

#### LBN Test
```bash
law run MulticlassEvaluation --version 20230710_lbn_prep --recipe txhx --year 2018 --training-version schwan_lbn_test --group-override default --lbn-particles 0
```

#### DNN Test
```bash
law run MulticlassEvaluation --version §change_me_please --recipe txhx --year 2018 --training-version schwan_test --group-override default --debug --lbn-particles 0
```

#### DNN Optimizer
```bash
law run MulticlassOptimizerPlot --version §change_me_please --opt-version §change_me_please --group-override §change_me_please --recipe txhx --n-initial 200 --n-parallel 100 --iterations 500 --workers 100 --remove-output 0,a,True
law run MulticlassOptStitchedEvaluation --version 20230825_new_base_with_bTagNorm --stitch-version 20230825_stitched --recipe txhx
```

#### DNN StitchedOptimized
```bash
law run MulticlassOptimizedStitched --version 20230710_lbn_prep --stitch-version 20230821_1st_campaign_3rd_new --recipe txhx --workers 5 --remove-output 0,a,True
```

#### DNN discrimination hists
```bash
law run PlotProducer --version §change_me_please --process-group unblinded --recipe txhx --year 2018 --blind-thresh 10000 --log-scale
```
- for all 4 strategies at once:
```bash
./plots_4_strats.sh VERSION_NAME COMMAND_TYPE
```

### DatacardProducer
```bash
law run DatacardProducer --version 20230823_stitched_1st_3rd_fix_empty_hists --recipe txhx --category ttH --variable dnn_score_max --year 2018 --binning-strategy equal_frequency_sig --transfer
```

### combine LXPLUS
```bash
mamba activate cmsx
cd inference_txhx/
source setup.sh inference
law run PlotLikelihoodTask --version 20230823_stitched_1st_3rd_fix_empty_hists --category ttH --binning equal_frequency_sig --variable dnn_score_max --LikelihoodScanTask-points 200 --campaign 2018_short --remove-output 1,a,1

```

### combine fits
- used for statistics
- in context of txHx e.g. for evaluation of most likely kappa_top values/sign
  
#### Likelihood analysis of latest ttH analysis
- [https://gitlab.cern.ch/cms-analysis/hig/HIG-21-006/datacards/-/tree/master](https://gitlab.cern.ch/cms-analysis/hig/HIG-21-006/datacards/-/tree/master)
```bash
combine <workspace file> -M MultiDimFit --setParameters kappa_V=1,kappa_t=1,kappa_tau=1,kappa_b=1,kappa_mu=1,kappa_c=1,kappa_ttilde=0,r=1,kappa_g=1,kappa_gam=1 -t -1 --redefineSignalPOIs kappa_t --freezeParameters kappa_V,kappa_tau,kappa_b,kappa_c,r,kappa_mu,kappa_g,kappa_gam,kappa_ttilde -m 125 --points 100 --X-rtd MINIMIZER_analytic --verbose 1 --algo grid --alignEdges 1 --robustFit 1 --saveNLL --robustHesse 1 --cminPreScan --cminDefaultMinimizerStrategy 0 --setParameterRanges kappa_t=-5.0,5.0
```
#### Asymptotic Limit on signal strength modifier
```bash
combineTool.py -M AsymptoticLimits  --setParameters kappa_V=1,kappa_t=1,kappa_tau=1,kappa_b=1,kappa_mu=1,kappa_c=1,kappa_ttilde=0,r=1,kappa_g=1,kappa_gam=1 -t -1 --redefineSignalPOIs r --freezeParameters kappa_V,kappa_tau,kappa_b,kappa_c,kappa_t,kappa_mu,kappa_g,kappa_gam,kappa_ttilde  workspace_all.root -m 125 --X-rtd MINIMIZER_MaxCalls=99999999999 --maxFailedSteps 500 --X-rtd MINIMIZER_analytic --setRobustFitTolerance 0.05 --cminPreScan --robustFit 1  --cminDefaultMinimizerStrategy 0
```
#### Asmptotic Limits for different kappa_T
- example how to implement in bash
```bash
for kt in -5..5; do combine ... --setParameters ...,kappa_t=${kt},... ...; done
```

### VISPA

- `cbq --help` not `man cbq` as it is an internal tool and not equal to the 'official' `cbq`

- `cbq -me -gpu`
  
- `cbq -me -past=3d` 
  
- `cbq *username* -gpu -kill -past=2`

- `cbq *username* -gpu -hist -limit 20`

## How to create plots with DNN discrimination score value

- `th_thh_tthh/multiclass/DNN.py`: assert that `build_dnn_cats` equals `True`
  - ```python
    class DNNBase:
    """Class containing the select function, apply categorisation"""

    build_dnn_cats = True
    ```
- `th_thh_tthh/config/variables.py` are the DNN plots defined:
  - ```python
        # single histogram with max dnn prediction output
    cfg.variables.add(
        aci.Variable(
            name="dnn_score_max",
            expression="np.max(pred, axis=-1)",
            binning=(400, 0.0, 1.0),
            unit="",
            x_title=r"DNN score",
            tags={"fit", "shifts", "dnn_score", "rebin"},
        )
    )
    ```

## binning strategies
- `th_tth_tthh/models/nlo.py` line 37:
  ```python
    @classproperty
    def binning_strategy(self):
        """defines how to bin histograms"""
        return "equal_width"
  ```

- `th_tth_tthh/models/common.py` line 210:
  ```python
    binning_functions = {
        "equal_width": equal_width_binnings,
        "equal_frequency_bkg": equal_frequency_binnings,
        "equal_frequency_sig": equal_frequency_binnings,
        "threshold": threshold_binnings,
    }
  ```

## CoffeaProcessor execution chain

### general

`law index` search files specified in `law.cfg` after `[modules]` for class inheriting from `law.Tasks`

### without specifying `--processor`

1. ```bash
    #CLI
    law run CoffeaProcessor
    ```
2. ```python
    #tasks/coffea.py
    output, metrics = processor.run_uproot_job(
    ...
    )
    
    ```
3. ```python
    #[package]/coffea/processor/executor.py
    def _work_function(
        ...
        out = processor_instance.process(events)
        ...
    )
    ```
4. ```python
    #th_tth_tthh/recipes/txhx.py
    class Processor(DNNBase, Base, Histogramer):
    ```

5. ```python
    #utils/coffea.py/Histogramer
    class Histogramer(BaseProcessor):
        ...
        def process(self, events):
            ...
            for _locals in self.select(events):
            ...
    ```
6. ```python
    #th_tth_tthh/recipes/txhx.py
    class Processor(DNNBase, Base, Histogramer):
    ```
7. ```python
    #th_tth_tthh/recipes/txhx.py
    class Base(common.Base):
            ...
            def select(self, events):
            ...
    ```

### with explicitly setting `--processor Exporter`

1. ```bash
    #CLI
    law run CoffeaProcessor --processor Exporter
    ```
2. see above
3. see above
4. ```python
    #th_tth_tthh/recipes/txhx.py
    class Exporter(Base, ArrayExporter):
    ```
5. ```python
    #utils/coffea.py
    class ArrayExporter(BaseProcessor):
    ...
        def process(self, events):
    ...
    ```

## Corrections that rerun if new Processing
- `BTagNorm`
- `PileupCorrections`
- `PDFCorrections`

## DNN code
1. ```python
   Network = getattr(tk, multiclass_inst.network_architecture)
   ```
   returns something like a constructor for either a `FullyConnected` or a `ResNet` and defines the corresponding substructure variable
   - `FullyConnected`: `substructure = DenseLayer`
   - `ResNet`: `substructure = ResNetBlock`

2. ```python
    class LinearNetwork(tf.keras.layers.Layer):
        ...
        def build(self, input_shape):
            network_layers = []
            for layer in range(self.layers):
                network_layers.append(self.substructure(**self.sub_kwargs))
            self.network_layers = network_layers
   ```
   in the `layer` for loop, layers of the `substructure` type are append to the `network_layers` list

3. The `substructure` layer inherits from the `BaseLayer` or `tf.keras.layers.Layer`. Exemplary, the `DenseLayer` is studied:
   ```python
   class DenseLayer(BaseLayer):
       part_order = ["weights", "bn", "act", "dropout"]

       def __init__(self, **kwargs):
           super().__init__(name="DenseLayer", **kwargs)