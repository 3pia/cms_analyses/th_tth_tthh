# -*- coding: utf-8 -*-

import tasks.corrections.processors as corr_proc


class Base:
    # "TTTo2L2Nu"  # "WWW_4F"  # "data_B_ee" # 2018: "data_B_e+ee"
    debug_dataset = "TTToSemiLeptonic"

    def get_pu_key(self, events):
        ds = self.get_dataset(events)
        if ds.is_data:
            return "data"
        else:
            return ds.name

    def variable_full_shifts(self, variable):
        """Return wether all shifts shall be written to histogram

        Returns
        -------
        bool
            if "shifts" in variable.tags
        """
        return "shifts" in variable.tags


class MCOnly:
    @classmethod
    def requires(cls, task):
        return task.base_requires(data_source="mc")


class PUCount(Base, corr_proc.PUCount):
    memory = "1000MiB"


def get_parameter_from_model_path(model_path: str, parameter: str) -> str:
    """Return parameter from dnn_model path.

    the dnn model path contains parameters of the model
    this function returns the value of the parameter if it is in the model path

    Returns
    -------
    str
        relevant parameter part in model path (self.dnn_model)
    """
    paths = model_path.split("/")
    parameter_value = [path for path in paths if path.startswith(parameter)]
    assert len(parameter_value) == 1, "parameter could not be read properly from model path"
    return parameter_value[0]
