# coding: utf-8

import warnings
from collections import defaultdict
from typing import List, Optional, Tuple

import awkward as ak
import hist as hist_pkg
import numpy as np
from hist import Hist
from tqdm import tqdm

import processor.util as util
import tasks.corrections.processors as corr_proc
from processor.generator import GeneratorHelper
from processor.sf import POGElectronSF, POGMuonSF
from tasks.corrections.btag import BTagSF_reshape
from utils.bh5 import Histogram as Hist5
from utils.coffea import ArrayExporter, Histogramer, PackedSelection, Weights

from ..multiclass.DNN import DNNBase
from . import common

btag_wp = {
    "legacy": {
        "2016": {"medium": 0.3093, "tight": 0.7221},
        "2017": {"medium": 0.3033, "tight": 0.7489},
        "2018": {"medium": 0.2770, "tight": 0.7264},
    }
}

SL_pt_cut = {
    "2016": {"electron": 29.0, "muon": 26.0},
    "2017": {"electron": 30.0, "muon": 29.0},
    "2018": {"electron": 30.0, "muon": 26.0},
}


def select_muons(muons):
    # muon selection here

    mu_selection = (
        muons.tightId
        & (muons.pfRelIso04_all <= 0.15)
        & (muons.pt >= 15.0)
        & (np.abs(muons.eta) <= 2.4)
    )

    return muons[mu_selection]


def select_electrons(electrons):
    # implement electron selection here

    ele_selection = (
        (electrons.cutBased == 4) & (electrons.pt >= 15.0) & (np.abs(electrons.eta) <= 2.4)
    )

    return electrons[ele_selection]


def select_jets(jets):
    # implement jet selection here
    # NOTE: for jet pileup id see: https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJetID

    jet_selection = (
        ak.values_astype(((jets.jetId >> 2) & 1), bool)
        & (jets.pt >= 30)
        & (np.abs(jets.eta) <= 2.4)
        & ((jets.puId >= 4) | (jets.pt >= 50))  # use puId only for jets with pt< 50
    )

    return jets[jet_selection]


def get_jets_based_on_deltaR(jets, maximum: bool = False):
    """get pair of jets that are either closest or farthest in Delta R (jet1, jet2)

    Parameters
    ----------
    jets : awkward array
        array of jets per event
    maximum : bool, optional
        if true the jet pair with maximum Delta R is returned
        else minimum Delta R
        by default False

    Returns
    -------
    tuple
        tuple of jets meeting the criteria
    """
    jet_combinations = ak.combinations(jets, 2, axis=1, fields=["j1", "j2"])
    jet1, jet2 = ak.unzip(jet_combinations)
    # deltaR = jet1.delta_r(jet2)
    if maximum:
        deltaR_index = ak.argmax(jet1.delta_r(jet2), axis=1, keepdims=True)
    else:
        deltaR_index = ak.argmin(jet1.delta_r(jet2), axis=1, keepdims=True)

    return jet1[deltaR_index], jet2[deltaR_index]


class Base(common.Base):
    @property
    def leading_jets_need_btag(self):
        return False

    @property
    def use_5fs_sample_only(self):
        return False

    @property
    def trigger(self):
        """
        run era
        2016 B-H
        2017 B-F
        2018 A-D
        """
        return {
            # NOTE: In the beginning, only data from 2018 is used. To use more data EXTEND the SELECTION where necessary!!!
            #
            # 2016: {
            #     "e": {
            #         "Ele27_WPTight_Gsf": all,
            #     },
            #     "mu": {
            #         "IsoMu24": all,
            #         "IsoTkMu24": all,
            #     },
            # },
            # TODO: add correct trigger combination for electron
            # !!! pay attention to analysis note
            # Ele32_WPTight_GsF_L1DoubleEG AND Ele32DoubleL1ToSingleL1Flag
            # 2017: {
            #     "e": {
            #         "Ele32_WPTight_GsF_L1DoubleEG": all
            #         "Ele28_eta2p1_WPTight_GsF_HT150": all
            #     },
            #     "mu": {
            #         "IsoMu27": all,
            #     },
            # },
            #
            2018: {
                "e": {
                    "Ele32_WPTight_Gsf": all,
                    "Ele28_eta2p1_WPTight_Gsf_HT150": all,
                },
                "mu": {
                    "IsoMu24": all,
                },
            },
        }[int(self.year)]

    @property
    def tensors(self):
        """
        needed for ArrayExporter
        Returns
        -------
        dict:
            'name': (id, number of elements, variables, type, aux)
        """
        common_variables = ("energy", "px", "py", "pz")  # , "pt", "eta", "phi"
        hl = self.hl
        year = ("year",)

        return {
            "jet": (
                "clean_good_b_jets",
                6,
                common_variables,
                np.float32,
                {"groups": ["multiclass", "eval", "input", "part"]},
            ),
            # eventnumber is needed for stitched networks
            # use correct subnetwork for event
            "eventnr": (
                None,
                0,
                ["eventnr"],
                np.int64,
                {"groups": ["multiclass", "eval", "split"]},
            ),
            "procid": (
                None,
                0,
                ["procid"],
                np.int32,
                {"groups": ["multiclass", "class"]},
            ),
            "hl": (None, 0, hl, np.float32, {"groups": ["multiclass", "eval", "input"]}),
            # "year": (None, 0, year, np.float32, {"groups": ["multiclass", "eval", "input"]}),
        }

    def arrays(self, X):
        """
        Returns
        -------
        dict:
            dict with names of objects defined in tensors
        """
        out = {}
        for name, tensor in self.tensors.items():
            ID, number, variables, typ, aux = tensor

            def preproc(x, n):
                """Preprocess arrays.
                awkward arrays are converted to numpy
                the shape of the arrays are adjusted, so that they have the same shape

                Parameters:
                x: array
                    input array
                n: int
                    number of elements per variable (number of columns of the array)

                Returns
                -------
                np.array:
                    output array
                    numpy array containing the values of the variables in the correct shape
                """
                return ak.to_numpy(
                    util.normalize(x, pad=1).flatten()
                    if n == 0
                    else ak.fill_none(ak.pad_none(x, n, clip=True), 0),
                    allow_missing=False,
                )

            if ID is None:
                vals = [preproc(X[variable], number) for variable in variables]
            else:
                vals = [preproc(getattr(X[ID], variable), number) for variable in variables]

            out[name] = np.stack(vals, axis=-1)
            # set all nans of infs to 0
            out[name] = np.nan_to_num(out[name], nan=0.0, posinf=0.0, neginf=-0.0).astype(typ)

        return out

    # list of high-level (hl) variables for the exporter to export
    hl_AN = [  # hl variables from analysis note
        "average_btag_all_jets",
        "variance_btag_all_jets",
        "average_btag_all_b_jets",
        "third_highest_btag_all_jets",
        "average_delta_eta_b_jets",
        "average_delta_eta_jets",
        "average_minv_all_b_jets",
        "average_minv_all_jets",
        "minv_bjets_closest_dR",
        "average_pt_all_jets",
        "average_pt_all_b_jets",
        "sum_pt_closest_b_jets",
        "sum_pt_all_jets",
        "sum_pt_all_b_jets",
    ]
    hl_additional = [  # hl variables not in analysis note
        "minv_bb_leading_btag_12",
        "minv_bb_leading_btag_23",
        "minv_bb_leading_pt_12",
        "minv_bb_leading_pt_23",
    ]
    hl = hl_AN + hl_additional

    def select(self, events):
        output = self.accumulator.identity()

        # get meta infos
        dataset_key = tuple(events.metadata["dataset"])
        dataset_inst = self.get_dataset(events)

        # filter NaNs arising from broken events with broken MET
        # e.g.: debug_dataset, debug_uuids = "WJetsToLNu_HT-100To200", {"DB4EE882-D7FB-984A-A1E0-D3E3DADCD19D"}, 1st chunk
        # Event No: 54867
        goodMET = np.isfinite(events.MET.pt)
        if not np.all(goodMET):
            warnings.warn(f"{np.sum(~goodMET)}/{len(events)} MET events are NaN, dropping them")
            events = events[goodMET]

        # filter bad PDF
        # if dataset_inst.is_mc:
        #     events = self.corrections["pdf"].filterBad(events)

        n_events = len(events)
        eventnr = ak.to_numpy(events.event)

        def const_arr(value):
            arr = np.broadcast_to(value, shape=(n_events,))
            arr.flags.writeable = False
            return arr

        # uuid - file finder debug variable
        uuid = self.get_lfn(events, default="").rsplit("/", 1)[-1].split(".", 1)[0].replace("-", "")
        uuid = [const_arr(int(uuid[i], 16) if i < len(uuid) else 17) for i in range(32)]

        dataset_id = dataset_inst.id * np.ones(n_events)

        weight_bad = const_arr(np.float32(np.nan))

        # early category definition
        common = ["good_vertices", "lumimask", "MET", "met_filter", "single_lepton"]
        cats = {
            "inclusive": ["min3bTags", "min4jets"],  # tH, ttH and ttHH
            "tH": ["exactly3bTags", "min4jets"],
            # "ttH": ["4to5bTags", "min5jets"],  # NOTE check exactly 4 correct, check jet criteria
            # "ttHH": ["min6bTags", "min6jets"],  # NOTE check jet criteria
            "ttH": ["min4btags", "min5jets"],  # NOTE changed for Thesis of Felix/Victor
        }
        # skip cuts with bTag requirement for btagnorm correction BTagSFNormEff
        self.skip_cuts = {"min3bTags", "exactly3bTags", "min4bTags"}  # "4to5bTags", "min6bTags"}

        # load objects
        muons = events.Muon
        electrons = events.Electron
        jets = events.Jet[ak.argsort(events.Jet.pt, ascending=False)]

        # trigger
        trigger = util.Trigger(events.HLT, self.trigger, dataset_inst)

        # split tt+jets background
        datasets = {None: 1}
        tt_subregions = ["bb", "2b", "b", "cc", "lf"]
        if dataset_inst.is_mc:
            # get generator weights
            gen_weights = events.Generator.weight

            # if dataset_shifts = True
            # there will be datasets with the same name, e.g. TTbb_4f_TTToSemiLeptonic
            # but they simulate a certain shift/uncertainty and NOT nominal
            # for the reweighting we only need nominal sum_gen_weights
            dataset_shift = self.get_dataset_shift(events)

            # extract genTtbarId
            # see https://twiki.cern.ch/twiki/bin/view/CMSPublic/GenHFHadronMatcher
            two_digit_ttbarId = events.genTtbarId % 100

            tt_b_mask = np.array(two_digit_ttbarId == 51)
            tt_2b_mask = np.array(two_digit_ttbarId == 52)
            tt_bb_mask = np.array((two_digit_ttbarId >= 53) & (two_digit_ttbarId <= 55))
            tt_lf_mask = np.array(two_digit_ttbarId == 00)
            tt_cc_mask = np.array((two_digit_ttbarId >= 41) & (two_digit_ttbarId <= 45))

            # masks have to be exclusive and exhaustive
            assert ak.all(
                (tt_b_mask + tt_2b_mask + tt_bb_mask + tt_lf_mask + tt_cc_mask)
                == np.ones_like(tt_b_mask)
            ), "tt+jets masks are not exhaustive or exclusive"

            if self.use_5fs_sample_only:
                if dataset_inst.name.startswith("TTTo"):
                    subset_name = dataset_inst.name.strip("TTTo")
                    datasets.update(
                        {
                            f"tt+b_{subset_name}": tt_b_mask,
                            f"tt+2b_{subset_name}": tt_2b_mask,
                            f"tt+bb_{subset_name}": tt_bb_mask,
                            f"tt+lf_{subset_name}": tt_lf_mask,
                            f"tt+cc_{subset_name}": tt_cc_mask,
                        }
                    )
                    for ps_region in tt_subregions:
                        dataset_name = f"tt+{ps_region}"
                        sgw = ak.sum(gen_weights[datasets[f"{dataset_name}_{subset_name}"]])
                        output["sum_gen_weights"][
                            (f"{dataset_name}_{subset_name}_5fs", f"{dataset_shift}_5fs")
                        ] = sgw
                        output["sum_gen_weights"][
                            (f"{dataset_name}_{subset_name}", dataset_shift)
                        ] = sgw

            else:
                if dataset_inst.name.startswith("TTTo"):
                    subset_name = dataset_inst.name.strip("TTTo")
                    datasets.update(
                        {
                            f"tt+b_{subset_name}": tt_b_mask,
                            f"tt+2b_{subset_name}": tt_2b_mask,
                            f"tt+bb_{subset_name}": tt_bb_mask,
                            f"tt+lf_{subset_name}": tt_lf_mask,
                            f"tt+cc_{subset_name}": tt_cc_mask,
                            f"tt+b_{subset_name}_5fs": tt_b_mask,
                            f"tt+2b_{subset_name}_5fs": tt_2b_mask,
                            f"tt+bb_{subset_name}_5fs": tt_bb_mask,
                        }
                    )
                    for ps_region in tt_subregions:
                        dataset_name = f"tt+{ps_region}"
                        sgw = ak.sum(gen_weights[datasets[f"{dataset_name}_{subset_name}"]])
                        output["sum_gen_weights"][
                            (f"{dataset_name}_{subset_name}_5fs", f"{dataset_shift}_5fs")
                        ] = sgw
                        if ps_region in ["cc", "lf"]:
                            output["sum_gen_weights"][
                                (f"{dataset_name}_{subset_name}", dataset_shift)
                            ] = sgw
                        else:
                            datasets.update(
                                {f"{dataset_name}_{subset_name}": np.zeros_like(tt_2b_mask)}
                            )

                if dataset_inst.name.startswith("TTbb_4f_TTTo"):
                    subset_name = dataset_inst.name.strip("TTbb_4f_TTTo")
                    # TTbb dataset name is written in lower case, change to upper case
                    if subset_name == "2l2nu":
                        subset_name = "2L2Nu"
                    datasets.update(
                        {
                            f"tt+b_{subset_name}": tt_b_mask,
                            f"tt+2b_{subset_name}": tt_2b_mask,
                            f"tt+bb_{subset_name}": tt_bb_mask,
                            f"tt+lf_{subset_name}": tt_lf_mask,
                            f"tt+cc_{subset_name}": tt_cc_mask,
                        }
                    )
                    for ps_region in tt_subregions:
                        dataset_name = f"tt+{ps_region}"
                        sgw = ak.sum(gen_weights[datasets[f"{dataset_name}_{subset_name}"]])
                        output["sum_gen_weights"][
                            (f"{dataset_name}_{subset_name}_4fs", f"{dataset_shift}_4fs")
                        ] = sgw
                        if ps_region in ["bb", "b", "2b"]:
                            output["sum_gen_weights"][
                                (f"{dataset_name}_{subset_name}", dataset_shift)
                            ] = sgw
                        else:
                            datasets.update(
                                {f"{dataset_name}_{subset_name}": np.zeros_like(tt_2b_mask)}
                            )
            if dataset_inst.name.startswith(("TTTo", "TTbb")):
                masks = {
                    f"tt+b_{subset_name}": tt_b_mask,
                    f"tt+2b_{subset_name}": tt_2b_mask,
                    f"tt+bb_{subset_name}": tt_bb_mask,
                    f"tt+lf_{subset_name}": tt_lf_mask,
                    f"tt+cc_{subset_name}": tt_cc_mask,
                }

        for unc, shift, met, jets, fatjets in self.jec_loop(events):
            weights = Weights(n_events, dtype=np.float32, storeIndividual=self.individual_weights)

            # build categories
            categories = {
                "_".join([cats_key]): common + cats_value for cats_key, cats_value in cats.items()
            }

            #
            # selection WITHIN events
            #

            # start object cutflow
            output["object_cutflow"][f"total_muons_{dataset_inst.name}"] += ak.sum(ak.num(muons))
            output["object_cutflow"][f"total_electrons_{dataset_inst.name}"] += ak.sum(
                ak.num(electrons)
            )
            output["object_cutflow"][f"total_jets_{dataset_inst.name}"] += ak.sum(ak.num(jets))

            # muons
            good_muons = select_muons(muons)
            good_muons = good_muons[ak.argsort(good_muons.pt, axis=1, ascending=False)]
            output["object_cutflow"][f"good_muons_{dataset_inst.name}"] += ak.sum(
                ak.num(good_muons)
            )

            # electrons
            good_electrons = select_electrons(electrons)
            good_electrons = good_electrons[ak.argsort(good_electrons.pt, axis=1, ascending=False)]
            output["object_cutflow"][f"good_electrons_{dataset_inst.name}"] += ak.sum(
                ak.num(good_electrons)
            )

            # jets
            good_jets = select_jets(jets)
            good_jets = good_jets[ak.argsort(good_jets.pt, axis=1, ascending=False)]
            output["object_cutflow"]["good_jets"] += ak.sum(ak.num(good_jets))
            # get HT from event
            ht = util.get_ht(good_jets)

            # combine muons and electrons
            good_leptons = ak.concatenate([good_muons, good_electrons], axis=1)
            good_leptons = good_leptons[ak.argsort(good_leptons.pt, axis=1, ascending=False)]

            # create arrays for single electron and single muon separately
            leading_e_pt = util.normalize(good_electrons.pt, pad=1, clip=True)[:, 0]
            ch_e = (
                (leading_e_pt > SL_pt_cut[self.year]["electron"])
                & trigger.get("e")
                & (ak.num(good_electrons) == 1)
                & (ak.num(good_muons) == 0)
            )
            leading_mu_pt = util.normalize(good_muons.pt, pad=1, clip=True)[:, 0]
            ch_mu = (
                (leading_mu_pt > SL_pt_cut[self.year]["muon"])
                & trigger.get("mu")
                & (ak.num(good_muons) == 1)
                & (ak.num(good_electrons) == 0)
            )

            # now clean all objects
            def cleanagainst_leptons(toclean, dr):
                return toclean[
                    util.nano_object_overlap(
                        toclean=toclean, cleanagainst=good_leptons[:, :2], dr=dr
                    )
                ]

            clean_good_jets = cleanagainst_leptons(good_jets, 0.4)
            clean_good_jets = clean_good_jets[
                ak.argsort(clean_good_jets.pt, axis=1, ascending=False)
            ]
            output["object_cutflow"]["clean good jets"] += ak.sum(ak.num(clean_good_jets))

            output["n_events"][dataset_key] = n_events

            # =========================
            # Baseline Event Selection
            # =========================

            selection = PackedSelection()

            # === corrections ===
            # phi MET modulation
            if "metphi" in self.corrections:
                met = self.corrections["metphi"](met, events.PV.npvs, dataset_inst)

            # --- if is data ---
            if dataset_inst.is_data:
                # --- sum gen weights ---
                output["sum_gen_weights"][dataset_key] = 0.0

                # --- lumimask ---
                selection.add("lumimask", self.corrections["lumimask"](events))

                # --- met filter ---
                met_filter = self.campaign_inst.aux["met_filters"]["data"]

            # --- if is MC ---
            if dataset_inst.is_mc:
                # # --- Event weights ---
                # weights.add("Event weights", events.genWeight)

                # --- pdf ---
                self.corrections["pdf"](events, weights, clip=10, relative=True)

                generatorHelper = GeneratorHelper(events, weights)
                generatorHelper.PSWeight(lfn=self.get_lfn(events))
                generatorHelper.ScaleWeight()

                # --- sum gen weights ---
                generatorHelper.gen_weight(output, dataset_key, datasets)
                # output["sum_gen_weights"][dataset_key] = ak.sum(gen_weights)
                # weights.add("gen_weight", gen_weights)

                # --- lumimask ---
                selection.add("lumimask", np.ones(n_events, dtype=bool))

                # --- met filter ---
                met_filter = self.campaign_inst.aux["met_filters"]["mc"]

                # --- lepton scale factors
                if "muon" in self.corrections:
                    POGMuonSF(self.corrections)(good_muons, weights, self.year)

                if "electron" in self.corrections:
                    # NOTE: from analysis note, scale factors have been derived
                    POGElectronSF(self.corrections)(good_electrons, weights)

                # AK4 Jet reshape SF
                BTagSF_reshape(self, events, weights, clean_good_jets, shift, unc)

                # jetPUId
                if "jetpuid" in self.corrections:
                    self.corrections["jetpuid"]["loose"](clean_good_jets, weights)

                # pileup
                if "pileup" in self.corrections:
                    weights.add(
                        "pileup",
                        **self.corrections["pileup"](
                            pu_key=self.get_pu_key(events), nTrueInt=events.Pileup.nTrueInt
                        ),
                    )

                # L1 ECAL prefiring
                if self.year in ("2016", "2017"):
                    weights.add(
                        "l1_ecal_prefiring",
                        events.L1PreFiringWeight.Nom,
                        weightUp=events.L1PreFiringWeight.Up,
                        weightDown=events.L1PreFiringWeight.Dn,
                    )

                # trigger
                if "trigger" in self.corrections:
                    lep_pt = util.normalize(good_muons.pt, pad=True)
                    lep_abseta = np.abs(util.normalize(good_muons.eta, pad=True))
                    corr_trig = self.corrections["trigger"]["hist"]

                    for lep_name, ch in [
                        ("muon", ch_mu),
                        ("electron", ch_e),
                    ]:
                        prefix = f"txHx_single_{lep_name}_trigger"
                        nominal = corr_trig[f"{prefix}_nominal"](lep_pt, lep_abseta)
                        up = corr_trig[f"{prefix}_up"](lep_pt, lep_abseta)
                        down = corr_trig[f"{prefix}_down"](lep_pt, lep_abseta)
                        assert ak.all(up >= nominal), "up shift has to be greater/equal to nominal"
                        assert ak.all(down <= nominal), "down shift has to be less/equal to nominal"
                        weights.add(
                            f"trigger_{lep_name}_sf",
                            ak.where(ch, nominal, 1),
                            weightUp=ak.where(ch, up, 1),
                            weightDown=ak.where(ch, down, 1),
                        )

            # === selection ===
            # --- good vertices ---
            number_pv = events.PV.npvsGood
            selection.add("good_vertices", number_pv >= 1)

            # --- met filter ---
            if self.year in ["2017", "2018"]:
                met_filter += ["ecalBadCalibFilterV2"]  # missing met filter
            met_filter_mask = util.nano_mask_and(events.Flag, met_filter)
            selection.add("met_filter", met_filter_mask)

            # --- MET pt ---
            selection.add("MET", met.pt >= 20)

            # --- single lepton ---
            sl_selection = ch_e | ch_mu

            if ak.any(ch_e & ch_mu):
                raise AssertionError("more than one lepton")

            selection.add("single_lepton", sl_selection)

            # === th ===
            # --- bTag ---
            """
            two different methods for the 3 b-tag requirement
            1. the three leading jets in pt also have to pass the b-tag requirement
            2. any of the jets in the event have to pass the b-tag requirement
            NOTE: first method decreases number of events significantly without better signal
            """

            clean_good_jets_btag = clean_good_jets.btagDeepFlavB
            btag_above_wp = clean_good_jets_btag >= btag_wp["legacy"][self.year]["medium"]

            clean_good_b_jets = clean_good_jets[btag_above_wp]
            clean_good_b_jets_btag = clean_good_jets_btag[btag_above_wp]

            n_btag = ak.sum(btag_above_wp, axis=-1)
            leading_jets_btag = (
                util.normalize(clean_good_jets_btag, pad=6, clip=True)
                >= btag_wp["legacy"][self.year]["medium"]
            )

            output["object_cutflow"]["clean_good_b_jets"] += ak.sum(n_btag)

            if self.leading_jets_need_btag:
                # leading #jets have to be b-tagged
                selection.add("min3bTags", ak.all(leading_jets_btag[:, :3], axis=1))
                selection.add(
                    "exactly3bTags", ak.all(leading_jets_btag[:, :3], axis=1) & n_btag == 3
                )
                # NOTE changed for Thesis of Felix/Victor
                selection.add("min4btags", (ak.all(leading_jets_btag[:, :4], axis=1) & n_btag >= 4))
                # selection.add(
                #     "4to5bTags",
                #     (ak.all(leading_jets_btag[:, :4], axis=1) & n_btag == 4)
                #     or (ak.all(leading_jets_btag[:, :5], axis=1) & n_btag == 5),
                # )
                # selection.add("min6bTags", ak.all(leading_jets_btag[:, :6], axis=1))
            else:
                # any number of jets have to be b-tagged
                selection.add("min3bTags", n_btag >= 3)
                selection.add("exactly3bTags", n_btag == 3)
                selection.add("min4btags", (n_btag >= 4))
                # selection.add("4to5bTags", (n_btag == 4) | (n_btag == 5))
                # selection.add("min6bTags", n_btag >= 6)

            # --- jets ---
            selection.add("min4jets", ak.num(clean_good_jets) >= 4)
            selection.add("min5jets", ak.num(clean_good_jets) >= 5)
            selection.add("min6jets", ak.num(clean_good_jets) >= 6)

            # output["object_cutflow"]["min4jets"] = ak.sum(ak.num(clean_good_jets) >= 4)
            # output["object_cutflow"]["min3bTags"] = ak.sum(n_btag >= 3)

            # output["object_cutflow"][f"min4jets_{dataset_inst.name}"] = ak.sum(
            #     ak.num(clean_good_jets) >= 4
            # )
            # output["object_cutflow"][f"min3bTags_{dataset_inst.name}"] = ak.sum(n_btag >= 3)

            # output["object_cutflow"]["selectionAll"] = ak.sum(
            #     selection.all(*categories["inclusive"])
            # )
            # output["object_cutflow"][f"selectionAll_{dataset_inst.name}"] = ak.sum(
            #     selection.all(*categories["inclusive"])
            # )
            # output["object_cutflow"][f"weights_{dataset_inst.name}"] = ak.sum(weights.weight())
            # output["object_cutflow"][f"weights_{dataset_inst.name}_cuts"] = ak.sum(
            #     weights.weight()[selection.all(*categories["inclusive"])]
            # )
            # fileid = (
            #     self.get_lfn(events, default="")
            #     .rsplit("/", 1)[-1]
            #     .split(".", 1)[0]
            #     .replace("-", "")
            # )
            # output["object_cutflow"][f"weights_{fileid}_{dataset_inst.is_data}"] = ak.sum(
            #     weights.weight()
            # )
            # output["object_cutflow"][f"weights_{fileid}_{dataset_inst.is_data}_cuts"] = ak.sum(
            #     weights.weight()[selection.all(*categories["inclusive"])]
            # )

            # === variables ===

            # average b tagging discriminant value of all jets
            average_btag_all_jets = ak.mean(clean_good_jets_btag, axis=-1)

            # variance of b tagging discriminant values of all jets
            variance_btag_all_jets = ak.var(clean_good_jets_btag, axis=-1)

            # average b tagging discriminant value of all b-tagged jets
            average_btag_all_b_jets = ak.mean(clean_good_b_jets_btag, axis=-1)

            # third highest b tagging discriminant value of all jets
            third_highest_btag_all_jets = util.normalize(
                clean_good_jets_btag[ak.argsort(clean_good_jets_btag, axis=-1, ascending=False)],
                pad=3,
                clip=True,
            )[:, 2]

            jet_combinations = ak.combinations(clean_good_jets, 2, axis=1, fields=["jet1", "jet2"])
            bjet_combinations = ak.combinations(
                clean_good_b_jets, 2, axis=1, fields=["jet1", "jet2"]
            )
            # average of delta eta between two b-tagged jets
            average_delta_eta_b_jets = ak.mean(
                np.abs(bjet_combinations.jet1.eta - bjet_combinations.jet2.eta), axis=-1
            )

            # average of delta eta between two jets
            average_delta_eta_jets = ak.mean(
                np.abs(jet_combinations.jet1.eta - jet_combinations.jet2.eta), axis=-1
            )

            # average invariant mass of all b-tagged jets
            average_minv_all_b_jets = ak.mean(clean_good_b_jets.mass, axis=-1)

            # average invariant mass of all jets
            average_minv_all_jets = ak.mean(clean_good_jets.mass, axis=-1)

            # get b-tagged jets closest in delta R
            bjet_1, bjet_2 = get_jets_based_on_deltaR(clean_good_b_jets, maximum=False)

            # invariant mass of pair of b-tagged jets closest in delta R
            minv_bjets_closest_dR = (bjet_1 + bjet_2).mass

            # average pt of all jets
            average_pt_all_jets = ak.mean(clean_good_jets.pt, axis=-1)

            # average pt of all b-tagged jets
            average_pt_all_b_jets = ak.mean(clean_good_b_jets.pt, axis=-1)

            # scalar sum of pt of pair of closest b-tagged jets
            sum_pt_closest_b_jets = bjet_1.pt + bjet_2.pt

            # scalar sum of pt of all jets pt
            sum_pt_all_jets = util.get_ht(clean_good_jets)

            # scalar sum of pt of all b-tagged jets
            sum_pt_all_b_jets = util.get_ht(clean_good_b_jets)

            # # --- not in analysis note ---
            # invariant mass of two b-tagged jets leading in b-tag value
            leading_btag_bjets = clean_good_b_jets[
                ak.argsort(clean_good_b_jets_btag, axis=-1, ascending=False)
            ]
            minv_bb_leading_btag_12 = leading_btag_bjets[:, :2].sum().mass

            eta_bb_leading_btag_12 = leading_btag_bjets[:, :2].sum().eta

            # invariant mass of two b-tagged jets second and third leading in b-tag value
            minv_bb_leading_btag_23 = leading_btag_bjets[:, 1:3].sum().mass

            # invariant mass of two b-tagged jets leading in pt
            leading_pt_bjets = clean_good_b_jets[
                ak.argsort(clean_good_b_jets.pt, axis=-1, ascending=False)
            ]
            minv_bb_leading_pt_12 = leading_pt_bjets[:, :2].sum().mass

            # invariant mass of two b-tagged jets second and third leading in pt
            minv_bb_leading_pt_23 = leading_pt_bjets[:, 1:3].sum().mass

            # get b-tagged jets farthest in delta R
            bjet_1, bjet_2 = get_jets_based_on_deltaR(clean_good_b_jets, maximum=True)

            # invariant mass of pair of b-tagged jets closest in delta R
            minv_bjets_maximum_dR = (bjet_1 + bjet_2).mass

            # === miscellaneous ===

            # Exporter needs procids, called in arrays()
            if dataset_inst.is_mc:
                if dataset_inst.name.startswith(("TTTo", "TTbb")):
                    """
                    we want to separate tt+jets processes into tt+b, tt+bb, ...
                    we need to get the process ids from the analysis instance
                    see th_tth_tthh/config/analysis.py for details on the processes
                    """
                    procid = np.zeros(n_events)
                    for key, mask in masks.items():
                        if key is not None:
                            procid[mask] = self.analysis_inst.processes.get(key).id
                    assert np.all(procid), "some processes have ID = 0!"
                else:
                    (process,) = dataset_inst.processes.values
                    procid = np.full(n_events, process.id)
            else:
                procid = np.full(n_events, self.analysis_inst.processes.get("data").id)

            yield locals()


class Processor(DNNBase, Base, Histogramer):
    # turns on jet energy corrections, runs ~11x longer
    jes_shifts = True
    # needed for uncertainty underlying_event, MEPSMatchingScale
    dataset_shifts = True
    memory = "3000MiB" if jes_shifts else "2500MiB"
    skip_cats = set()

    # === MC ===
    debug_dataset = "TTToSemiLeptonic"
    debug_uuids = [
        "377EEF14-6B1B-B044-8186-7377E4063F1E",
        "B0C1142D-D1D5-734F-BD22-8698EEC58095",
        "98F9548F-93D8-5240-A654-F304575A307E",
    ]  # year 2018

    # === Data ===
    # debug_dataset = "data_A_mu"
    # debug_uuids = [
    #     "C0CFEC3C-2A75-8048-AF1A-157C54686E75",
    #     "6F705C5E-1632-3240-92BA-8EF13184A8D5",
    # ]  # year 2018

    # we need additional 5fs processes for tt+B
    @classmethod
    def additional_group_processes(cls, task):
        if "tt+B" in task.process_group:
            return ["tt+B_5fs"]
        else:
            return ["tt+bb_5fs", "tt+2b_5fs", "tt+b_5fs"]

    # set if DNN should be used in processing
    @property
    def use_dnn(self):
        return True

    @classmethod
    def requires(cls, task):
        return task.base_requires(
            # data_source="mc",
            electron={},
            muon={},
            jet={},
            btag={},
            btagnorm={},
            pileup={},
            trigger={},
            pdf={},  # "version": "20230825_new_base_with_bTagNorm"
            jetpuid={},
            metphi={},
            lumimask={},
            corrections=False,
        )

    @classmethod
    def reweight_histograms(cls, hists, task):
        tt_finalstates = ["Hadronic", "SemiLeptonic", "2L2Nu"]
        tt_subregions = ["tt+bb", "tt+2b", "tt+b", "tt+cc", "tt+lf"]

        inp = task.input()["processor"].load()
        sum_gen_weights = inp["sum_gen_weights"]

        for variable, hist in tqdm(
            hists.items(), unit="variable", desc="reweighting of tt background"
        ):
            if variable not in [v.name for v in task.analysis_inst.variables]:
                continue

            hist_out = hist.copy()
            hist_view = hist.view(True)
            view_out = hist_out.view(True)
            assert view_out.dtype.names == ("value", "variance")

            # NOTE: gen weights give correct ratios only within one dataset
            # we need to process the three final state datasets seperately
            for finalstate in tt_finalstates:
                # calculate total sum_gen_weights for one final state but all subregions
                sum_gen_weights_total = sum(
                    [
                        sum_gen_weights[(f"{region}_{finalstate}_5fs", "nominal_5fs")]
                        for region in tt_subregions
                    ]
                )

                # correct crosssection of subregions based on fraction in 5FS
                for subregion in tt_subregions:
                    dataset_name = f"{subregion}_{finalstate}"

                    xsec_rw = (
                        sum_gen_weights[(f"{dataset_name}_5fs", "nominal_5fs")]
                        / sum_gen_weights_total
                    )
                    assert (
                        0.0 <= xsec_rw <= 1.0
                    ), "Cross section reweighting factor is not between 0 and 1"

                    # index: dataset name, category, "nominal", histogram
                    assert dataset_name in hist.axes["dataset"], dataset_name
                    hv = hist_view[hist.axes["dataset"].index(dataset_name)]
                    assert hist_view.dtype.names == ("value", "variance")
                    hv["value"] *= xsec_rw
                    hv["variance"] *= xsec_rw**2

                    view_out[hist.axes["dataset"].index(dataset_name)] = hv

                    if subregion not in ["tt+bb", "tt+2b", "tt+b"]:
                        continue

                    assert f"{dataset_name}_5fs" in hist.axes["dataset"], f"{dataset_name}_5fs"
                    dataset_index = hist.axes["dataset"].index(f"{dataset_name}_5fs")
                    hv = hist_view[dataset_index]
                    assert hist_view.dtype.names == ("value", "variance")
                    hv["value"] *= xsec_rw
                    hv["variance"] *= xsec_rw**2

                    view_out[dataset_index] = hv

            hist_out.view(True)[...] = view_out
            hists[variable] = hist_out

        return hists

    @classmethod
    def group_processes(cls, hists, task):
        # after group.py called reweight_histograms and group.py executed the grouping, we need to add the top variations

        # redefine topvariations to account for splitted tt+jets background
        def topvariation(
            hist: Hist,
            processes: List[str],
            src: str,
            dst: Optional[str] = None,
            symmetrize: bool = True,
            scale: float = 1.0,
        ) -> Hist:
            """
            Returns:
            Hist including `dst{Up,down}`  variations from `src`

            If `src` is directly available, it is intrepreted as `dstUp` and `dstDown` is set to
            `nominal` or the reflection (about `nominal`) of `src` depending on `symmetrize`
            , otherise `src{Up,Down}` are used for `dst{Up,down}`.

            The output `dst{Up,down}` are `scale` (w.r.t nominal), i.e. negative vales flip and 0 disables
            the shift.
            """
            if dst is None:
                dst = src

            hnew = Hist5.regrow(
                hist,
                {"systematic": [f"{dst}Up", f"{dst}Down"]},
                copy=False,
            )
            newUpIdx = hnew.axes["systematic"].index(f"{dst}Up")
            newDownIdx = hnew.axes["systematic"].index(f"{dst}Down")

            hv = hist.view(True)
            for process in processes:
                ttIdx = hist.axes["process"].index(process)
                syst = hist.axes["systematic"]
                assert f"{dst}Up" not in syst
                assert f"{dst}Down" not in syst

                nomIdx = syst.index("nominal")
                if src in syst:
                    upIdx = downIdx = syst.index(src)
                    upScale = scale
                    downScale = -scale if symmetrize else 0
                else:
                    upIdx = syst.index(f"{src}Up")
                    downIdx = syst.index(f"{src}Down")
                    upScale = downScale = scale
                if len(syst) in {upIdx, downIdx}:
                    continue  # return hnew

                tt = hv[ttIdx, ...]
                nominal = tt[:, nomIdx, :]
                up = tt[:, upIdx, :]
                down = tt[:, downIdx, :]

                # fmt: off
                # nominal
                hnew.view(True)[ttIdx, :, newUpIdx, :]["value"] = nominal["value"] + upScale * (up["value"] - nominal["value"])
                hnew.view(True)[ttIdx, :, newDownIdx, :]["value"] = nominal["value"] + downScale * (down["value"] - nominal["value"])

                # variance propagation
                hnew.view(True)[ttIdx, :, newUpIdx, :]["variance"] = nominal["variance"] + upScale**2 * (up["variance"] + nominal["variance"])
                hnew.view(True)[ttIdx, :, newDownIdx, :]["variance"] = nominal["variance"] + downScale**2 * (down["variance"] + nominal["variance"])
                # fmt: on
            return hnew

        def topvariations(
            hist: Hist,
            processes: List[str],
            progress: Optional[dict] = dict(desc="topvariations"),
            symmetrize: bool = True,
        ) -> Hist:
            """
            Returns a new histogram containing only the top variation systematic uncertainties

            Can calculate the following variations:

            UnderlyingEvent:
                Directly inferred from up/down variations of the datasets

            TopMass (3 GeV shift):
                Up = 175.5 GeV
                Down = 169.5 GeV

            ColorReconnection:
                for cr in GluonMove, QCDbased, erdON:
                    Up = cr
                    Down = (nominal - (up - nomina)) if symmetrize else nominal

            MEPSMatchingScale:
                Directly inferred from up/down variations of the datasets
            """

            variations = [
                # dataset name(s), systematic name, scale factor
                dict(src="TuneCP5", dst="UnderlyingEvent"),
                dict(src="mtop", dst="TopMass", scale=1.0 / 3.0),  # 3 GeV shift -> 1 GeV shift
                dict(src="GluonMove"),
                dict(src="QCDbased"),
                dict(src="erdON"),
                dict(src="HDamp", dst="MEPSMatchingScale"),
            ]
            systematics = [
                (var.get("dst", var["src"]) + d) for var in variations for d in ["Up", "Down"]
            ]

            # clone histogram, create new shifts
            out = Hist5.regrow(
                hist,
                {"systematic": systematics},
                copy=False,
            )

            # get view of histograms
            hv = hist.view(True)
            ov = out.view(True)

            # set new shifts to nominal
            ov[:, :, :, :] = hv[:, :, [hist.axes[2].index("nominal")], :]

            # set new shifts for ttbar to 0
            for process in processes:
                ov[[out.axes[0].index(process)], :, :, :] *= 0

            # set new shifts for ttbar to explicit value
            for kwargs in (tq := tqdm(variations, desc="topvariation")):
                tq.set_postfix(**kwargs)
                out += topvariation(hist=hist, processes=processes, symmetrize=symmetrize, **kwargs)
            return out

        def adjust_underlying_event_unc(hist: Hist, processes: List[Tuple[str, ...]]) -> Hist:
            """Adjust Underlying event Uncertainty.

            The relative up/down shifts of the 5FS dataset due to UnderlyingEvent uncertainty
            is calculated and applied to the 4FS dataset
            (compare Analysis Note)

            Parameters
            ----------
            hist:
                histogram from CoffeaProcessor
            processes: List[Tuple[str,...]]
                processes that need to be adjusted

            Returns
            -------
            Hist:
                histogram with the applied uncertainty changes

            """

            h_view = hist.view(True)
            remaining_processes = set(hist.axes["process"])
            for process, process_5fs in processes:
                assert process in hist.axes["process"], f"{process} not in {hist.axes['process']}"
                assert (
                    process_5fs in hist.axes["process"]
                ), f"{process_5fs} not in {hist.axes['process']}"
                proc_index = hist.axes["process"].index(process)
                proc_5fs_index = hist.axes["process"].index(process_5fs)
                systematic = hist.axes["systematic"]
                assert "UnderlyingEventUp" in systematic
                assert "UnderlyingEventDown" in systematic

                upIdx = systematic.index("UnderlyingEventUp")
                downIdx = systematic.index("UnderlyingEventDown")
                nomIdx = systematic.index("nominal")

                proc = h_view[proc_index, ...]
                nominal = proc[:, nomIdx, :]

                proc_5fs = h_view[proc_5fs_index, ...]
                nominal_5fs = proc_5fs[:, nomIdx, :]
                up_5fs = proc_5fs[:, upIdx, :]
                down_5fs = proc_5fs[:, downIdx, :]

                # relative uncertainty taken from 5fs and applied to 4fs
                # NOTE: from AN,
                # we apply the average of the absolute up/down rate changes as uncertainty
                # symmetrically around the nominal. In order to increase the statistical precision of the
                # estimate, we use the average relative variation over all three years

                rel_up = up_5fs["value"] / nominal_5fs["value"]
                rel_down = down_5fs["value"] / nominal_5fs["value"]
                # when nominal is 0 but up/down has a value, rel is inf
                rel_up[np.abs(rel_up) == np.inf] = np.nan
                rel_down[np.abs(rel_down) == np.inf] = np.nan
                # in case of nominal == 0, rel_up is nan
                # replace nan with average shift
                # NOTE: https://stackoverflow.com/questions/18689235/numpy-array-replace-nan-values-with-average-of-columns
                rel_up_ma = np.ma.array(rel_up, mask=np.isnan(rel_up))
                rel_up = np.where(np.isnan(rel_up), rel_up_ma.mean(axis=1)[:, np.newaxis], rel_up)
                rel_down_ma = np.ma.array(rel_down, mask=np.isnan(rel_down))
                rel_down = np.where(
                    np.isnan(rel_down), rel_down_ma.mean(axis=1)[:, np.newaxis], rel_down
                )

                h_view[proc_index, :, upIdx, :]["value"] = rel_up * nominal["value"]
                h_view[proc_index, :, downIdx, :]["value"] = rel_down * nominal["value"]

                h_view[proc_index, :, upIdx, :]["variance"] = rel_up**2 * nominal["variance"]
                h_view[proc_index, :, downIdx, :]["variance"] = rel_down**2 * nominal["variance"]

                h_view

                remaining_processes -= {process_5fs}

            # remove 5fs processes, they shall not be passed to following tasks
            remaining_processes = list(remaining_processes)
            hist_out = hist_pkg.Hist(
                hist_pkg.axis.StrCategory(remaining_processes, name="process", growth=True),
                *hist.axes[1:],
                metadata=hist.metadata,
                storage=hist_pkg.storage.Weight(),
            )
            hist_out_view = hist_out.view(True)
            hist_out_view[hist_out.axes["process"].index(remaining_processes), ...] = h_view[
                hist.axes["process"].index(remaining_processes), ...
            ]

            return hist_out

        for variable, hist in tqdm(
            hists.items(), unit="variable", desc="add MEPS, UnderlyingEvent uncertainties"
        ):
            var = task.analysis_inst.variables.get(variable)
            if not var:
                # skip variables not in variables.py
                continue
            if "shifts" not in var.tags:
                # skip if shifts are not computed
                continue
            tt_processes = [proc for proc in hist.axes["process"] if "tt+" in proc or proc == "tt"]
            contains_tt_processes = len(tt_processes) > 0
            if contains_tt_processes and cls.dataset_shifts:
                hist += topvariations(hist, tt_processes)

                # get 4fs UnderlyingEvent uncertainty from 5fs processes
                # hist[proc, category, unc, bins]
                assert not all(
                    p in hist.axes["process"] for p in ["tt+B", "tt+bb", "tt+2b", "tt+b"]
                ), "tt+B and (tt+bb,tt+2b,tt+b) must not be in the histogram simultanously"
                hist = adjust_underlying_event_unc(
                    hist,
                    [
                        (proc.strip("_5fs"), proc)
                        for proc in cls.additional_group_processes(task)
                        if proc in hist.axes["process"]
                    ],
                )

        return hists


class Exporter(Base, ArrayExporter):
    debug_dataset = "TTToSemiLeptonic"

    sep = "#"
    group = "classification"

    memory = "4000MB"

    @classmethod
    def requires(cls, task):
        return task.base_requires(data_source="mc", btagnorm=False)

    @classmethod
    def live_callback(cls, accumulator):
        ret = defaultdict(int)
        for cat, num in super().live_callback(accumulator).items():
            ret[cat.split(cls.sep, 1)[0]] += num
        return ret

    def select(self, events):
        out = next(super().select(events))

        out["categories"] = {"inclusive": out["categories"]["inclusive"]}
        """
        ['good_vertices',
        'lumimask',
        'MET',
        'met_filter',
        'single_lepton',
        'min3bTags',
        'min4jets']
        """

        datasets = out.setdefault("datasets", {})
        datasets.setdefault(None, 1)
        for dataset_name, dataset_factor in datasets.items():
            dataset = (
                self.get_dataset(events)
                if dataset_name is None
                else self.campaign_inst.datasets.get(dataset_name)
            )
            (process,) = dataset.processes.values
            try:
                weight = process.xsecs[self.campaign_inst.ecm].nominal
            except KeyError:
                raise LookupError(
                    "error occured in dataset: " + str(dataset_name) + "\n process: " + str(process)
                )

            weight *= self.campaign_inst.aux["lumi"]
            if weight > 0:
                datasets[dataset_name] = dataset_factor * weight
            else:
                warnings.warn(
                    f"xsec*lumi weight of {weight} for datasets {dataset} is not positive"
                )

        yield out

    @classmethod
    def save(cls, *args, **kwargs):
        kwargs["sep"] = cls.sep
        return super().save(*args, **kwargs)


class BTagSFNormEff(Base, corr_proc.BTagSFNormEff):
    # we dont use subjets in the analysis, therefore commented out
    # subjets = "fatjets.subjets"
    debug_dataset = "TTToSemiLeptonic"

    @classmethod
    def requires(cls, task):
        return task.base_requires(
            data_source="mc", btagnorm=False, btagsubjet=False, dyestimation=False
        )

    # def select(self, *args, **kwargs):
    #     for ret in super().select(*args, **kwargs):
    #         cats = ret["categories"]
    #         for cat in list(cats.keys()):
    #             if "_0b_" in cat or cat.endswith("_fr"):
    #                 del cats[cat]
    #         yield ret


class PUCount(Base, corr_proc.PUCount):
    memory = "1000MiB"
