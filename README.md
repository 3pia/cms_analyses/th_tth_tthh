# tH+ttH+ttHH analysis
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

- internally abbreviated as txHx
- most stuff done by Felix, Mate, Peter and Victor
- ttHH samples are included but not used due to lack of background modeling

## txHx specific branches
| repo        | branch               |
| ----------- | -------------------- |
| common      | txHx_muon_trigger_SF |
| tools       | txhx                 |
| th_tth_tthh | dnn                  |

## latest versions

### Array Exporter
```bash
--version 20230825_new_base_with_bTagNorm
```
- previous
```bash
--version 20230710_lbn_prep
```

### DNN score plots (equal frequency signal + 3)
- latest 6-classes:
```bash
--version 20230904_all_corrections_6classes
```
- latest 4-classes:
```bash
--version 20230829_all_corrections_4classes
```
- previous:
```bash
--version 20230823_stitched_1st_3rd_fix_empty_hists
```
- command to get plots for all 4 binning strategies and datacards for all strategies, categories and variables
```bash
law run PlotsCards --version VERSION_NAME --year 2018
```

### DatacardProducer (VISPA)
```bash
law run DatacardProducer --version 20230823_stitched_1st_3rd_fix_empty_hists --recipe txhx --category ttH --variable dnn_score_max --year 2018 --binning-strategy equal_frequency_sig --transfer
```

### Combine (LXPLUS)
```bash
source setup.sh NAME
law run PlotLikelihoodTask --version 20230823_stitched_1st_3rd_fix_empty_hists --category ttH --binning equal_frequency_sig --variable dnn_score_max --LikelihoodScanTask-points 200 --campaign 2018_short --remove-output 1,a,1
```

#### N Minus One
```bash
law run PlotNMinusOneTask --version 20230829_all_corrections_4classes --binning equal_width --variable dnn_score_max --category tH:ttH --points 30 --scan-range 10 --campaign 2018_short --remove-output 1,a,1
```
- final result:
```bash
--category tH_dnn_node_tH:tH_dnn_node_ttH:tH_dnn_node_tt+jets:tH_dnn_node_others
```

## further documentation
- Detailed documentation on the use of the multiclass code see `victor.md`