"""File contains code related to the DNN used in txhx analysis"""

import logging

import numpy as np

from processor.util import reduce_or
from utils.tf_serve import autoClient

from ..recipes.common import get_parameter_from_model_path


class DNNBase:
    """Class containing the select function, apply categorisation"""

    build_dnn_cats = True

    def __init__(self, task):
        super().__init__(task)
        self.logger = logging.getLogger("luigi-interface")

        self.dnn = autoClient(self.dnn_model, remote=False)

    @property
    def dnn_model(self):
        """Path to DNN Model to use"""
        try:
            return self.analysis_inst.aux["dnn_model"]
        except KeyError as key_error:
            raise KeyError(
                f"dnn model path not defined in analysis {self.analysis_inst.name}"
            ) from key_error

    @property
    def multiclass_splits(self) -> int:
        """Return the number of sub networks.

        If the network ist stitched the parameter split is non-zero
        in case of no stitching, the eventnumber must not be passed as additional input

        Returns
        -------
        int
            number of subnetworks (Multiclasstraining.split)
        """

        splits = get_parameter_from_model_path(self.dnn_model, "multiclass_splits_").split("_")[2]
        return int(splits)

    @property
    def multiclass_group(self) -> str:
        """Return multiclass group from used dnn model.

        The name of the used group is stored in the model path

        Returns
        -------
        str
            name of the multiclass group used in the DNN model
        """

        group = get_parameter_from_model_path(self.dnn_model, "multiclass_group_").split("_")[2:-1]
        return "_".join(group)

    @property
    def multiclass_processes(self):
        """Return processes as defined in analysis.py

        Returns
        -------
        dict
            dictionary with the classes as keys
        """
        multiclass_config = self.analysis_inst.aux["multiclass"]
        # group = multiclass_config.group
        return multiclass_config.groups[self.multiclass_group]

    # overwrites function in utils/coffea.py in Histogramer
    # def category_variables(self, category) -> list:
    # """ define variables to be histed for certain category"""
    #     if "_dnn_node_" in category:
    #         return [v for v in self.variables if v.name.startswith("dnn_score_max")]
    #     else:
    #         return self.variables

    def variable_full_shifts(self, variable) -> bool:
        """Return wether all shifts shall be written to histogram

        Returns
        -------
        bool
            if "shifts" in variable.tags
        """
        return "shifts" in variable.tags

    @property
    def use_dnn(self):
        """Return whether to use DNN in analysis or not"""
        return False

    def select(self, *args, **kwargs):
        """Extend select function from Base class in txhx.py .

        This select function applies the DNN
        """
        self.logger.info(f"multiclass classes: {self.multiclass_processes.keys()}")

        for ret in super().select(*args, **kwargs):
            if not self.use_dnn:
                yield ret

            else:
                selection = ret["selection"]
                categories = ret["categories"]

                # DNN score:
                mask = reduce_or(*(selection.all(*cuts) for cuts in categories.values()))
                arr = self.arrays(ret)
                dnn_inputs = {}
                for key, value in arr.items():
                    if key in self.tensors:
                        groups = self.tensors[key][-1].get("groups", [])
                        if "multiclass" in groups and "eval" in groups:
                            dnn_inputs[key] = value[mask]

                if self.multiclass_splits == 0:
                    # delete eventnr key from dnn_inputs
                    dnn_inputs.pop("eventnr", None)

                (pm,) = self.dnn(inputs=dnn_inputs).values()
                target_shape = (mask.sum(), len(self.multiclass_processes))
                assert pm.shape == target_shape, (pm.shape, target_shape)
                assert not np.any(np.isnan(pm)), "DNN produces nan values"
                pred = np.full(mask.shape[:1] + pm.shape[1:], np.nan, dtype=pm.dtype)
                pred[mask] = pm
                pred_max = np.argmax(pred, axis=-1).astype(
                    np.float16
                )  # all events with mask[event_index] == False get classified as class 0, not optimal # fmt: skip
                pred_max[
                    ~mask
                ] = np.nan  # to avoid accidential classification of not predicted samples
                catkeys = [c for c in categories.keys() if not (set(c.split("_")) & self.skip_cats)]
                if self.build_dnn_cats:
                    for index, dnn_class in enumerate(self.multiclass_processes.keys()):
                        # check which class is predicted
                        m = pred_max == index
                        class_name = f"dnn_node_{dnn_class}"
                        # add class prediction to selection
                        selection.add(class_name, m)
                        # add dnn category
                        for key in catkeys:
                            cuts = categories[key]
                            cuts_dnn = cuts + [class_name]
                            categories[f"{key}_{class_name}"] = cuts_dnn
                ret["pred"] = pred
                yield ret
