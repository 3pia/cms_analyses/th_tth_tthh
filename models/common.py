# coding: utf-8

import os
from collections import UserDict
from typing import Dict, List
import logging

import hist
import numpy as np
from tasks.binning import Rebin
from tqdm.auto import tqdm
from utils.binning import wquant, thresh_rebin
from utils.datacard import Datacard, RDict


# resolution of the type float, needed to circumvent rounding errors
machine_eps = 1e-10  # np.finfo(float).eps


class UncertaintyDict(UserDict):
    """
    dict class for uncertainties
    uncertainties are either single values or 2-tuples
    values are in percent
    in case for 2-tuples: (down shift, up shift)

    Return:
    -------
    - 1 + value/100 for single values
    - (1 - value[0]/100, 1 + value[1]/100) for 2-tuples
    """

    def __getitem__(self, key):
        val = super().__getitem__(key)
        if type(val) == tuple:
            return (1 - val[0] / 100, 1 + val[1] / 100)
        else:
            return 1 + val / 100


class BinningMethods:
    logger = logging.getLogger("luigi-interface")

    def remove_boundary_zeros(bin_content):
        """Remove zeros at the beginning and the end of an array.

        Parameters
        ----------
        bin_content : numpy.array
            1D array to trim

        Returns
        -------
        bin_content : numpy.array
            trimmed array
        bounds : tuple
            index of the (lower, upper) boundary elements == 0
        """
        assert bin_content.ndim == 1, "bin_content array not 1D"
        assert np.sum(bin_content) > 0, "all bins are empty wtf"
        zeros = bin_content != 0
        # zeros.argmax gets the index of the first element != 0 from the left
        lower = int(zeros.argmax())
        # zeros[::-1].argmax gets the index of the first element != 0 from the right
        upper = int(zeros.size - zeros[::-1].argmax())

        return bin_content[lower:upper], (lower, upper)

    def equal_width_binnings(cls, variable, histogram, task):
        # NOTE: should we do some sophisticated binning method ?
        # number of bins can be set in variable.aux
        # use {"rebin_nbins": <number>}
        binnings = {}
        for category in tqdm(
            histogram.axes["category"],
            desc=f"calculate binning - {variable.name}",
            unit="category",
            leave=False,
        ):
            # hist_view = histogram.view()[:, histogram.axes[1].index(category)].copy()
            # hist_nominal = hist_view[:, histogram.axes["systematic"].index("nominal"), :]
            # _, (lower, upper) = cls.remove_boundary_zeros(np.sum(hist_nominal, axis=0)["value"])

            bin_edges = histogram.axes[variable.name].edges  # [lower : upper + 1]

            # n_bins = 10
            # if "rebin_nbins" in variable.aux:
            #     n_bins = variable.aux["rebin_nbins"]
            n_bins = variable.aux.get("rebin_nbins", 10)
            # rebin to bins of equal width
            binning = np.linspace(bin_edges[0], bin_edges[-1], num=n_bins + 1)
            binnings[category] = binning.tolist()
        return binnings

    def equal_frequency_binnings(cls, variable, histogram, task):
        """Calculate bin edges such that background bin heights are almost equal

        Parameters
        ----------
        variable :
            variable of the histogram
        histogram :
            histogram corresponding to variable
        task :
            law task

        Returns
        -------
        dict
            contains the bin edges for each category
        """
        binnings = {}
        categories = histogram.axes["category"]
        for category in tqdm(
            categories, unit="category", desc=f"calculate binning - {variable.name}", leave=False
        ):
            hist_view = histogram.view()[:, histogram.axes[1].index(category)].copy()
            hist_nominal = hist_view[:, histogram.axes["systematic"].index("nominal"), :]
            # # get indices of bins with no entries
            # _, (lower, upper) = cls.remove_boundary_zeros(np.sum(hist_nominal, axis=0)["value"])

            bin_centers = histogram.axes[variable.name].centers  # [lower:upper]
            bin_edges = histogram.axes[variable.name].edges  # [lower : upper + 1]

            assert any(
                string in cls.binning_strategy for string in ["bkg", "sig"]
            ), f"binning strategy {cls.binning_strategy} does not contain 'bkg' or 'sig'"

            bkg_procs = [
                proc for proc in histogram.axes["process"] if (proc in cls.background_processes)
            ]
            sig_procs = [
                proc for proc in histogram.axes["process"] if (proc in cls.signal_processes)
            ]

            if "bkg" in cls.binning_strategy:
                rebin_procs = bkg_procs
            else:
                rebin_procs = sig_procs

            rebin_index = histogram.axes["process"].index(rebin_procs)

            # NOTE: number of bins hard coded, maybe use tag in variables instead ?
            n_bins = variable.aux.get("rebin_nbins", 50)
            # get sum of processes per bin
            bin_content = np.sum(hist_nominal[rebin_index], axis=0)["value"]
            # get bin edges so that processes are flat
            # scipy.interpolate needs at least two entries
            if len(bin_content) < 2:
                # cls.logger.warning("To few bin entries, could not calculate equal frequency bins")
                binnings[category] = list(histogram.axes[variable.name].edges)
                continue

            # decrease bin number until sufficient background
            loc = histogram.axes[variable.name].index
            success = False
            while not success:
                quantile = wquant(bin_centers, bin_content, np.r_[0 : 1 : int(n_bins + 1) * 1j])
                index = np.digitize(quantile, bin_edges) - 1
                new_binning = np.r_[bin_edges[0], bin_edges[index], bin_edges[-1]]
                # get sum of background events per new bin
                bkg = []
                for ind in range(len(new_binning) - 1):
                    bkg.append(
                        hist_nominal[
                            histogram.axes["process"].index(bkg_procs),
                            loc(new_binning[ind]) : loc(new_binning[ind + 1]),
                        ].sum()["value"]
                    )
                if np.all(np.array(bkg) > 10) or len(new_binning) <= 2:
                    success = True
                else:
                    n_bins -= 1

            new_binning = np.unique(new_binning)
            binnings[category] = list(new_binning)

        return binnings

    def threshold_binnings(cls, variable, histogram, task):
        """
        Calculate binning such that each bin fullfills
        (counts - uncertainty) > threshold
        if rsut( relative stat. unc. threshold) given, also
        (uncertainty / counts) <= rsut
        """
        binnings = {}
        categories = histogram.axes["category"]
        for category in tqdm(
            categories, unit="category", desc=f"calculate binning - {variable.name}", leave=False
        ):
            hist_view = histogram.view()[:, histogram.axes[1].index(category)].copy()
            hist_nominal = hist_view[:, histogram.axes["systematic"].index("nominal"), :]
            # get indices of bins with no entries
            # _, (lower, upper) = cls.remove_boundary_zeros(np.sum(hist_nominal, axis=0)["value"])

            bin_centers = histogram.axes[variable.name].centers  # [lower:upper]
            bin_edges = histogram.axes[variable.name].edges  # [lower : upper + 1]

            bkg_procs = [
                proc for proc in histogram.axes["process"] if (proc in cls.background_processes)
            ]
            bkg_index = histogram.axes["process"].index(bkg_procs)
            bkgarr = np.sum(hist_nominal[bkg_index], axis=0)["value"]  # [lower:upper]
            bkgvar = np.sum(hist_nominal[bkg_index], axis=0)["variance"]  # [lower:upper]
            main_bg = ["tt+lf", "tt+b", "tt+2b", "tt+bb", "tt+cc", "st"]
            assert all(
                bg in histogram.axes["process"] for bg in main_bg
            ), f"any of {main_bg} not in {histogram.axes['process']}"

            # get indices for bin edges, such that threshold of uncertainty is passed
            threshold = 10 * np.ones_like(bin_centers[:-1])
            # NOTE: you will get at max number of bins of len(threshold)
            # min bin content 10, this is threshold for automcstats
            bin_index = thresh_rebin(
                threshold,
                bkgarr,
                var=bkgvar,
                extra=np.c_[
                    tuple(
                        hist_nominal[histogram.axes["process"].index(bg)]["value"]  # [lower:upper]
                        for bg in main_bg
                    )
                ],
                rsut=0.05,
            )
            new_binning = np.r_[bin_edges[0], bin_edges[bin_index], bin_edges[-1]]
            # uniquify - no 0-width bins allowed
            new_binning = np.unique(new_binning)
            binnings[category] = list(new_binning)

        return binnings

    binning_functions = {
        "equal_width": equal_width_binnings,
        "equal_frequency_bkg": equal_frequency_binnings,
        "equal_frequency_sig": equal_frequency_binnings,
        "threshold": threshold_binnings,
    }


class StatModelBase(Datacard, BinningMethods):
    """StatModelBase for different StatModels.

    NOTE: cls in functions is StatModel from th_tth_tthh.models.nlo
    """

    # rename processes for datacard
    @property
    def rprocesses(self) -> RDict:
        return RDict(
            ttH="ttH_SM_hbb", THQ_ctcvcp_4f_Hincl="tHq_SM_hbb", THW_ctcvcp_5f_Hincl="tHW_SM_hbb"
        )

    @property
    def custom_lines(self) -> List[str]:
        # inspired by bbww_sl
        # rate param for freely-floating background normalization
        # rate_processes = ("tt", "st", "wjets")
        # for p in rate_processes:
        #     if p not in self.vps:
        #         continue

        # p = "tt+B"
        lines = []
        for p in ["tt+b", "tt+bb", "tt+2b"]:
            pname = self.rprocesses.get(p)
            rateParam_name = f"CMS_txHx_{pname}_norm"
            lines += [f"{rateParam_name} rateParam * {pname} 1 [0,5]"]

        lines += [f"{self.bin} autoMCStats 10 0 1"]  # defaults for last two parameters are 0 1

        # decorrelate uncertainties with same name
        procs = {
            self.rprocesses.get("ttH"): "ttH",
            self.rprocesses.get("THQ_ctcvcp_4f_Hincl"): "tHq",
            self.rprocesses.get("THW_ctcvcp_5f_Hincl"): "tHW",
        }
        procs.update({key: "tt+B" for key in ["tt+b", "tt+2b", "tt+bb"]})
        procs.update({key: "tt5fs" for key in ["tt+cc", "tt+lf"]})
        for systematic in ["ScaleWeight_Fact", "ScaleWeight_Renorm"]:
            lines += self.decorrelate(
                systematic=systematic,
                processes=procs,
            )

        procs = {self.rprocesses.get("ttH"): "ttH"}
        procs.update({key: "tt+B" for key in ["tt+b", "tt+2b", "tt+bb"]})
        procs.update({key: "tt5fs" for key in ["tt+cc", "tt+lf"]})
        for systematic in ["PSWeight_FSR", "PSWeight_ISR"]:
            lines += self.decorrelate(
                systematic=systematic,
                processes=procs,
            )

        procs = {
            self.rprocesses.get("THQ_ctcvcp_4f_Hincl"): "tHq",
            self.rprocesses.get("THW_ctcvcp_5f_Hincl"): "tHW",
        }
        procs.update({key: "tt+B" for key in ["tt+b", "tt+2b", "tt+bb"]})
        procs.update({key: "ttH+tt5fs" for key in ["ttH", "tt+cc", "tt+lf"]})
        for systematic in ["PDFSet_off", "PDFSet_rel"]:
            lines += self.decorrelate(
                systematic=systematic,
                processes=procs,
            )

        return lines

    def build_systematics(self):
        tt_subregions = ["tt+b", "tt+bb", "tt+2b", "tt+cc", "tt+lf"]

        uncertainties = {
            "ttH": UncertaintyDict({"scales_ttH": (9.2, 5.8), "pdf_gg_ttH": 3.6}),
            "tHq": UncertaintyDict({"scales_tHq": (14.9, 6.5), "pdf_qb_tHq": 3.7}),
            "tHW": UncertaintyDict({"scales_tHW": (6.7, 4.9), "pdf_gb_tHW": 6.3}),
            "tt": UncertaintyDict({"scales_tt": (3.5, 2.4), "pdf_gg": 4.2}),
            "st": UncertaintyDict({"scales_t": (2.1, 3.1), "pdf_gq": 2.8}),
            "wjets": UncertaintyDict({"scales_V": 3.8, "pdf_qq": (0.4, 0.8)}),
            "zjets": UncertaintyDict({"scales_V": 2.0, "pdf_qq": 0.2}),
            "ttW": UncertaintyDict({"scales_tt": (16.4, 25.5), "pdf_qq": 3.6}),
            "ttZ": UncertaintyDict({"scales_tt": (9.3, 8.1), "pdf_gg": 3.5}),
            "diboson": UncertaintyDict({"scales_VV": 3.0, "pdf_qq": 5.0}),
            "tt+2b": UncertaintyDict({"gluonsplitting": 100.0}),
        }

        # lumi https://gitlab.cern.ch/hh/naming-conventions#luminosity

        lumi = {
            2016: {
                "lumi_13TeV_2016": 1.010,
                "lumi_13TeV_correlated": 1.006,
            },
            2017: {
                "lumi_13TeV_2017": 1.020,
                "lumi_13TeV_correlated": 1.009,
                "lumi_13TeV_1718": 1.006,
            },
            2018: {
                "lumi_13TeV_2018": 1.015,
                "lumi_13TeV_correlated": 1.020,
                "lumi_13TeV_1718": 1.002,
            },
        }

        self.add_systematics(
            names=lumi[int(self.year)],
            type="lnN",
            processes=set(self.processes),
        )

        # process xsec uncertainties
        # Branching ratio lnN:
        # https://gitlab.cern.ch/hh/naming-conventions#branching-ratios
        # self.add_systematics(
        #     names={"BR_H_BB": (0.9874, 1.0124)},
        #     type="lnN",
        #     processes=set([p for p in self.processes if "2B" in p or "BB" in p] + ["ttH", "TH"]),
        # )

        # ttH
        self.add_systematics(
            names={
                # "alpha_s": 1.02,
                # "pdf_Higgs_ttH": 1.030,
                # "QCDscale_ttH": (0.908, 1.058),
                "QCDscale_ttH": uncertainties["ttH"]["scales_ttH"],
                "pdf_gg_ttH": uncertainties["ttH"]["pdf_gg_ttH"],
            },
            type="lnN",
            processes=["ttH"],
        )
        # THQ_ctcvcp_4f_Hincl
        self.add_systematics(
            names={
                # "alpha_s": 1.012,
                # "pdf_Higgs_ttH": 1.035,
                # "QCDscale_ttH": (0.853, 1.065),
                "pdf_qb_tHq": uncertainties["tHq"]["pdf_qb_tHq"],
                "QCDscale_THQ": uncertainties["tHq"]["scales_tHq"],
            },
            type="lnN",
            processes=["THQ_ctcvcp_4f_Hincl"],
        )
        # THW_ctcvcp_5f_Hincl
        self.add_systematics(
            names={
                # "alpha_s": 1.015,
                # "pdf_Higgs_ttH": 1.061,
                # "QCDscale_ttH": (0.933, 1.049),
                "pdf_gb_tHW": uncertainties["tHW"]["pdf_gb_tHW"],
                "QCDscale_THW": uncertainties["tHW"]["scales_tHW"],
            },
            type="lnN",
            processes=["THW_ctcvcp_5f_Hincl"],
        )

        # tt
        self.add_systematics(
            names={
                # "m_top_unc_tt": self.reluncs("tt", uncs="mtop"),
                "pdf_gg": uncertainties["tt"]["pdf_gg"],
                "QCDscale_ttbar": uncertainties["tt"]["scales_tt"],
            },
            type="lnN",
            processes=tt_subregions,
        )
        # tt+2b
        self.add_systematics(
            names={"gluonsplitting": uncertainties["tt+2b"]["gluonsplitting"]},
            type="lnN",
            processes=["tt+2b"],
        )
        # st
        self.add_systematics(
            names={
                "pdf_gq": uncertainties["st"]["pdf_gq"],
                "QCDscale_t": uncertainties["st"]["scales_t"],
            },
            type="lnN",
            processes=["st"],
        )
        # W + jets
        self.add_systematics(
            names={
                "pdf_qq": uncertainties["wjets"]["pdf_qq"],
                "QCDscale_V": uncertainties["wjets"]["scales_V"],
            },
            type="lnN",
            processes=["wjets"],
        )
        # Z + jets
        self.add_systematics(
            names={
                "pdf_qq": uncertainties["zjets"]["pdf_qq"],
                "QCDscale_V": uncertainties["zjets"]["scales_V"],
            },
            type="lnN",
            processes=["dy"],
        )
        # ttW
        self.add_systematics(
            names={
                "pdf_qq": uncertainties["ttW"]["pdf_qq"],
                "QCDscale_ttbar": uncertainties["ttW"]["scales_tt"],
            },
            type="lnN",
            processes=["ttW"],
        )
        # ttZ
        self.add_systematics(
            names={
                "pdf_gg": uncertainties["ttZ"]["pdf_gg"],
                "QCDscale_ttbar": uncertainties["ttZ"]["scales_tt"],
            },
            type="lnN",
            processes=["ttZ"],
        )
        # Diboson
        self.add_systematics(
            names={
                "pdf_qq": uncertainties["diboson"]["pdf_qq"],
                "QCDscale_VV": uncertainties["diboson"]["scales_VV"],
            },
            type="lnN",
            processes=["vv"],
        )

        #
        # # weighted VV uncs:
        # self.add_systematics(
        #     names={
        #         "pdf_qqbar": self.reluncs("vv", uncs="pdf"),
        #         "QCDscale_VV": self.reluncs("vv", uncs="scale"),
        #     },
        #     type="lnN",
        #     processes=["vv"],
        # )
        # self.add_systematics(
        #     names={
        #         "pdf_gg": self.reluncs("ttZ", uncs="pdf"),
        #         "QCDscale_ttbar": self.reluncs("ttZ", uncs="scale"),
        #         "alpha_s": self.reluncs("ttZ", uncs="alpha_s"),
        #         "EW_corr_ttZ": (0.998, 1.0),  # https://arxiv.org/pdf/1610.07922.pdf table 40
        #     },
        #     type="lnN",
        #     processes=["ttZ"],
        # )
        # self.add_systematics(
        #     names={
        #         "pdf_qqbar": self.reluncs("ttW", uncs="pdf"),
        #         "QCDscale_ttbar": self.reluncs("ttW", uncs="scale"),
        #         "alpha_s": self.reluncs("ttW", uncs="alpha_s"),
        #         "EW_corr_ttW": (0.968, 1.0),  # https://arxiv.org/pdf/1610.07922.pdf table 40
        #     },
        #     type="lnN",
        #     processes=["ttW"],
        # )
        # # weighted VVV uncs:
        # self.add_systematics(
        #     names={
        #         "total_VVV": self.reluncs("vvv", uncs="total"),
        #     },
        #     type="lnN",
        #     processes=["vvv"],
        # )

        # Shape Uncertainties
        # pileup, l1 ecal prefiring
        common_shape_systs = [
            "pileup",
        ]
        if self.year in (2016, 2017):
            common_shape_systs += ["l1_ecal_prefiring"]
        self.add_systematics(
            names=common_shape_systs,
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )

        # renormalization/factorisation scale
        # independent for ttH, tHq, tHW, tt+B(4FS), other tt(5FS)
        self.add_systematics(
            names=[
                f"ScaleWeight_Fact",
                f"ScaleWeight_Renorm",
                # "ScaleWeight_Mixed",
                # f"ScaleWeight_Envelope",
            ],
            type="shape",
            strength=1.0,
            processes=["ttH", "THQ_ctcvcp_4f_Hincl", "THW_ctcvcp_5f_Hincl"] + tt_subregions,
        )

        # PDF Set
        self.add_systematics(
            names=["PDFSet_off", "PDFSet_rel"],
            type="shape",
            strength=1.0,
            processes=["ttH", "THQ_ctcvcp_4f_Hincl", "THW_ctcvcp_5f_Hincl"] + tt_subregions,
        )

        # PS Scale ISR, FSR
        # independent for ttH, tt+B(4FS), other tt(5FS)
        self.add_systematics(
            names=[
                f"PSWeight_FSR",
                f"PSWeight_ISR",
            ],
            type="shape",
            strength=1.0,
            processes=["ttH"] + tt_subregions,
        )

        # ME-PS matching, Underlying event
        for proc in tt_subregions:
            self.add_systematics(
                names=[f"MEPSMatchingScale", "UnderlyingEvent"],
                type="shape",
                strength=1.0,
                processes=[proc],
            )

        # jet PU-ID
        self.add_systematics(
            names="jet_PUid_*",
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )

        # jes and jer uncertainties
        jes_uncs = self.campaign_inst.aux["jes_sources"] + ["jer"]
        self.add_systematics(
            names=jes_uncs,
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )

        # b-Tag
        self.add_systematics(
            names="btagWeight_*",
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )

        # trigger
        self.add_systematics(
            names="trigger*",
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )

        # lepton efficiencies
        self.add_systematics(
            names=["electron_id", "electron_reco"],
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )
        self.add_systematics(
            names=["muon_id", "muon_iso"],
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )

    @classmethod
    def calculate_binnings(cls, variable, h, task) -> Dict[str, List[float]]:
        # this method exists in order to be able to do no rebinning
        # without having to change the tags in variables.py
        if "rebin" in variable.tags and not (cls.binning_strategy == "no_rebin"):
            bin_strat = "equal_width"
            if cls.binning_strategy:
                bin_strat = cls.binning_strategy
            assert bin_strat in cls.binning_functions.keys(), "binning function not defined"

            return cls.binning_functions[bin_strat](
                cls=cls, variable=variable, histogram=h, task=task
            )
        else:
            return super().calculate_binnings(variable, h, task)

    @classmethod
    def rebin(cls, variable, h, binnings, task) -> Dict[str, hist.Hist]:
        if "rebin" in variable.tags:
            hists = {}
            for c in tqdm(
                h.axes["category"],
                desc=f"rebin - {variable.name}",
                unit="category",
                leave=False,
            ):
                hview = h.view(flow=False)[:, h.axes[1].index(c)].copy()
                hnew = hist.Hist(
                    h.axes["process"],
                    h.axes["systematic"],
                    hist.axis.Variable(
                        binnings[c],
                        name=variable.name,
                        label=variable.x_title,
                    ),
                    storage=hist.storage.Weight(),
                    metadata=h.metadata,
                )

                loc = h.axes[variable.name].index
                ax = hnew.axes[variable.name]
                for bin in range(ax.size):
                    hnew[..., bin] = hview[
                        ...,
                        loc(ax[bin][0] + machine_eps) : loc(ax[bin][1] + machine_eps),
                    ].sum(axis=-1)

                    assert loc(ax[bin][0] + machine_eps) != loc(
                        ax[bin][1] + machine_eps
                    ), "bin numbers are the same, check rebinning"

                _, (lower, upper) = cls.remove_boundary_zeros(hnew[sum, "nominal", :].values())

                hists[c] = hnew[:, :, lower:upper]
            return hists
        else:
            """
            no on wants to rebin

            Returns:
                dict: dictionary with histograms
            """
            hists = {}
            for c in tqdm(
                h.axes["category"],
                desc=f"no rebinning - {variable.name}",
                unit="category",
                leave=False,
            ):
                hists[c] = h[:, c, :, :]
            return hists

    @classmethod
    def requires(cls, task):
        procs = list(cls.processes) + ["data"]
        # NOTE: add processes so that they get plotted
        return Rebin.req(task, process_group=procs, trim_binnings=False)
