<!-- title: Systematic Uncertainties -->

List of Systematic Uncertainties as listed in AN2019_094_v15
============================================================
From Chapter 9 in Analysis Note
> - Use ValidateDatacards.py script to check datacard
> - shape uncertainties with "no real shape effect" are converted to lnN parameters

---

- [List of Systematic Uncertainties as listed in AN2019\_094\_v15](#list-of-systematic-uncertainties-as-listed-in-an2019_094_v15)
- [Notes](#notes)
- [Uncertainties](#uncertainties)
  - [Renorm./fact. scales](#renormfact-scales)
  - [PDF + $\\alpha\_s$](#pdf--alpha_s)
    - [PDF + $\\alpha\_s(gg)$](#pdf--alpha_sgg)
    - [PDF + $\\alpha\_s (q\\bar{q})$](#pdf--alpha_s-qbarq)
    - [PDF + $\\alpha\_s(qg)$](#pdf--alpha_sqg)
    - [Tables](#tables)
  - [Collinear gluon splitting](#collinear-gluon-splitting)
  - [$\\mu\_R$ scale](#mu_r-scale)
  - [$\\mu\_F$ scale](#mu_f-scale)
  - [PDF shape](#pdf-shape)
  - [PS scale ISR](#ps-scale-isr)
  - [PS scale FSR](#ps-scale-fsr)
  - [ME-PS matching ($t\\bar{t}$)](#me-ps-matching-tbart)
  - [Underlying event ($t\\bar{t}$)](#underlying-event-tbart)
  - [Integrated luminosity](#integrated-luminosity)
  - [Lepton ID/ISO](#lepton-idiso)
  - [Trigger efficiency](#trigger-efficiency)
  - [L1 prefiring correction](#l1-prefiring-correction)
  - [Pileup](#pileup)
  - [Jet energy](#jet-energy)
    - [Jet energy scale (11 scheme)](#jet-energy-scale-11-scheme)
    - [Jet energy resolution](#jet-energy-resolution)
  - [b tag](#b-tag)
    - [HF/LF fraction](#hflf-fraction)
    - [HF/LF stat (linear)](#hflf-stat-linear)
    - [HF/LF stat (quadratic)](#hflf-stat-quadratic)
    - [charm (linear)](#charm-linear)
    - [charm (quadratic)](#charm-quadratic)
    - [TF(loose) correction](#tfloose-correction)
    - [Size of the MC samples](#size-of-the-mc-samples)
    - [Jet Pileup](#jet-pileup)


---
# Notes
**Type**

R: rate

S: shape

**Correlation**

correlation between years 2016 - 2018

---
# Uncertainties
---
## Renorm./fact. scales
- Type R, correlated, Scale uncertainty of (N)NLO prediction, independent for ttH, tHq, tHW, tt, t, V, VV
- [x] in StatModel
- [x] correlations between processes
- [ ] correlations between years

inclusive cross-section uncertainties due to renormalisation and factorisation scales

Uncertainties in the same column for two different processes (rows) are treated as correlated

| Process |   Scales(ttH)   |   Scales(tHq)    |   Scales(tHW)   | Status  |
| :-----: | :-------------: | :--------------: | :-------------: | :-----: |
|   ttH   | +5.8 % / -9.2 % |                  |                 | &check; |
|   tHq   |                 | +6.5 % / -14.9 % |                 | &check; |
|   tHW   |                 |                  | +4.9 % / -6.7 % | &check; |


| Process  |    Scales(tt)     |    Scales(t)    |  Scales(V)  | Scales(VV) | Status  |
| :------: | :---------------: | :-------------: | :---------: | :--------: | :-----: |
|    tt    |  +2.4 % / -3.5 %  |                 |             |            | &check; |
| single t |                   | +3.1 % / -2.1 % |             |            | &check; |
| W + jets |                   |                 | $\pm$ 3.8 % |            | &check; |
| Z + jets |                   |                 |  $\pm$ 2 %  |            | &check; |
|   ttW    | +25.5 % / -16.4 % |                 |             |            | &check; |
|   ttZ    |  +8.1 % / -9.3 %  |                 |             |            | &check; |
| Diboson  |                   |                 |             | $\pm$ 3 %  | &check; |

---
## PDF + $\alpha_s$
### PDF + $\alpha_s(gg)$
- Type R, correlated, PDF uncertainty for gg initiated processes, independent for ttH, tHq, tHW, and others

### PDF + $\alpha_s (q\bar{q})$
- Type R, correlated, PDF uncertainty of $q\bar{q}$ initiated processes (ttW, W, Z) except tHq

### PDF + $\alpha_s(qg)$
- Type R, correlated, PDF uncertainty of qg initiated processes (single t) except tHW

### Tables
Inclusive cross-section uncertainties due to PDF + $\alpha_s$
- [x] in StatModel
- [x] correlations between processes
- [ ] correlations between years

Uncertainties for the same initial state(column) for two different processes(rows) are treated as correlated

|       |   gg_ttH    |   qb_tHq    |   gb_tHW    | Status  |
| :---: | :---------: | :---------: | :---------: | :-----: |
|  ttH  | $\pm$ 3.6 % |             |             | &check; |
|  tHq  |             | $\pm$ 3.7 % |             | &check; |
|  tHW  |             |             | $\pm$ 6.3 % | &check; |


|          |     gg      |       qq        |     gq      | Status  |
| :------: | :---------: | :-------------: | :---------: | :-----: |
|    tt    | $\pm$ 4.2 % |                 |             | &check; |
| single t |             |                 | $\pm$ 2.8 % | &check; |
| W + jets |             | -0.4 % / +0.8 % |             | &check; |
| Z + jets |             |   $\pm$ 0.2 %   |             | &check; |
|   ttW    |             |   $\pm$ 3.6 %   |             | &check; |
|   ttZ    | $\pm$ 3.5 % |                 |             | &check; |
| Diboson  |             |    $\pm$ 5 %    |             | &check; |

---
## Collinear gluon splitting
- type R, correlated, additional 100 % rate uncertainty on $t\bar{t}$+2b
- [x] implemented
- [x] in StatModel

---
## $\mu_R$ scale
- type S, correlated, Renormalisation scale uncertainty of ME generator, independent for ttH, tHq, tHW, tt+B(4FS), other tt(5FS)
- [ ] implemented
- [ ] in StatModel

---
## $\mu_F$ scale
- type S, correlated, Factorisation scale uncertainty of ME generator, independent for ttH, tHq, tHW, tt+B(4FS), other tt(5FS)
- [ ] implemented
- [ ] in StatModel

---
## PDF shape
- type S, correlated, from NNPDF variations, independent for tt+B(4FS), tHq, tHW and ttH/other tt(5FS)
- [ ] implemented
- [ ] in StatModel

---
## PS scale ISR
- type S, correlated, initial state radiation uncertainty of the PS(Pythia) independent for ttH, tt+B(4FS), other tt(5FS)
- [ ] implemented
- [ ] in StatModel
---
## PS scale FSR
- type S, correlated, final state radiation uncertainty of the PS(Pythia), independent for ttH, tt+B(4FS), other tt(5FS)
- [ ] implemented
- [ ] in StatModel

---
## ME-PS matching ($t\bar{t}$)
- type R, correlated, NLO ME to PS matching (for tt+jets events), independent for additional jet flavours
- [ ] implemented
- [ ] in StatModel

---
## Underlying event ($t\bar{t}$)
- type R, correlated, underlying event (for all tt+jets events)
- [ ] implemented
- [ ] in StatModel

> **Note**: no dedicated samples for tt+bb(4FS), check AN2019_094_v15 for more details

```python
class Processor(Base, Histogramer):
    # corresponds to "underlying_event" uncertainty in analysis note
    dataset_shifts = False
```

---
## Integrated luminosity
- [x] implemented
- [x] in StatModel
- type R, partially, signal and all backgrounds

txhx.py
```python
lumi = {
    2016: {
        "lumi_13TeV_2016": 1.010,
        "lumi_13TeV_correlated": 1.006,
    },
    2017: {
        "lumi_13TeV_2017": 1.020,
        "lumi_13TeV_correlated": 1.009,
        "lumi_13TeV_1718": 1.006,
    },
    2018: {
        "lumi_13TeV_2018": 1.015,
        "lumi_13TeV_correlated": 1.020,
        "lumi_13TeV_1718": 1.002,
    },
}
```
models.common.py
```python
self.add_systematics(
    names=lumi[int(self.year)],
    type="lnN",
    processes=set(self.processes),
)
```

---
## Lepton ID/ISO
- [x] implemented
- [x] in StatModel
- type S, uncorrelated, signal and all backgrounds
- muon_idDown/Up
- muon_isoDown/Up
- electron_idDown/Up
- electron_recoDown/Up

txhx.py
```python
# --- lepton scale factors
if "muon" in self.corrections:
    POGMuonSF(self.corrections)(good_muons, weights, self.year)

if "electron" in self.corrections:
    # NOTE: from analysis note, scale factors have been derived
    POGElectronSF(self.corrections)(good_electrons, weights)
```
models.common.py
```python
# lepton efficiencies
self.add_systematics(
    names=["electron_id", "electron_reco"],
    type="shape",
    strength=1.0,
    processes=set(self.processes),
)
self.add_systematics(
    names=["muon_id", "muon_iso"],
    type="shape",
    strength=1.0,
    processes=set(self.processes),
)
```

---
## Trigger efficiency
- [x] implemented
- [x] in StatModel
- type S, uncorrelated, signal and all backgrounds
- trigger_electron_sfDown/Up
- trigger_muon_sfDown/Up

txhx.py
```python
if "trigger" in self.corrections:
    lep_pt = util.normalize(good_muons.pt, pad=True)
    lep_abseta = np.abs(util.normalize(good_muons.eta, pad=True))
    corr_trig = self.corrections["trigger"]["hist"]

    for lep_name, ch, nom_str, up_str, down_str in [
        ("muon", ch_mu, "nominal", "up", "down"),
        (
            "electron",
            ch_e,
            "ele28_ht150_OR_ele32_ele_pt_ele_sceta",
            "ele28_ht150_OR_ele32_ele_pt_ele_sceta_error",
            "ele28_ht150_OR_ele32_ele_pt_ele_sceta_error",
        ),
    ]:
        prefix = f"txHx_single_{lep_name}_trigger"
        nominal = corr_trig[f"{prefix}_{nom_str}"](lep_pt, lep_abseta)
        up = corr_trig[f"{prefix}_{up_str}"](lep_pt, lep_abseta)
        down = corr_trig[f"{prefix}_{down_str}"](lep_pt, lep_abseta)
        weights.add(
            f"trigger_{lep_name}_sf",
            ak.where(ch, nominal, 1),
            weightUp=ak.where(ch, up, 1),
            weightDown=ak.where(ch, down, 1),
        )
```
models.common.py
```python
# trigger
self.add_systematics(
    names="trigger*",
    type="shape",
    strength=1.0,
    processes=set(self.processes),
)
```

---
## L1 prefiring correction
- [x] implemented
- [x] in StatModel
- type S, uncorrelated, signal and all backgrounds

txhx.py
```python
# L1 ECAL prefiring
if self.year in ("2016", "2017"):
    weights.add(
        "l1_ecal_prefiring",
        events.L1PreFiringWeight.Nom,
        weightUp=events.L1PreFiringWeight.Up,
        weightDown=events.L1PreFiringWeight.Dn,
    )
```

---
## Pileup
- [x] implemented
- [x] in StatModel
- type S, correlated, signal and all backgrounds

txhx.py
```python
# pileup
if "pileup" in self.corrections:
    weights.add(
        "pileup",
        **self.corrections["pileup"](
            pu_key=self.get_pu_key(events), nTrueInt=events.Pileup.nTrueInt
        ),
    )
```

models.common.py
```python
# pileup, l1 ecal prefiring
common_shape_systs = [
    "pileup",
]
if self.year in (2016, 2017):
    common_shape_systs += ["l1_ecal_prefiring"]
self.add_systematics(
    names=common_shape_systs,
    type="shape",
    strength=1.0,
    processes=set(self.processes),
)
```

---
## Jet energy
### Jet energy scale (11 scheme)
- [x] implemented
- [x] in StatModel
- type S, partially, signal, $t\bar{t}$+jets and Single-t

### Jet energy resolution
- [x] implemented
- [x] in StatModel
- type S, uncorrelated, Signal, $t\bar{t}$+jets and Single-t

txhx.py
```python
for unc, shift, met, jets, fatjets in self.jec_loop(events):
    # selection happening here

###

class Processor(Base, Histogramer):
    # turns on jet energy corrections, runs ~11x longer
    jes_shifts = False
```
models.common.py
```python
jes_uncs = self.campaign_inst.aux["jes_sources"] + ["jer"]
self.add_systematics(
    names=jes_uncs,
    type="shape",
    strength=1.0,
    processes=set(self.processes),
)
```

---
## b tag
- [x] implemented
- [x] in StatModel
- ```btagWeight_*```

### HF/LF fraction
- btagWeight_hf/lfDown/Up
- type S, partially, Signal and all backgrounds

### HF/LF stat (linear)
- btagWeight_hf/lfstats1Down/Up
- type S, uncorrelated, Signal and all backgrounds

### HF/LF stat (quadratic)
- btagWeight_hf/lfstats2Down/Up
- type S, uncorrelated, Signal and all backgrounds

### charm (linear)
- btagWeight_cferr1Down/Up 	
- type S, partially, Signal and all backgrounds

### charm (quadratic)
- btagWeight_cferr2Down/Up
- type S, partially, Signal and all backgrounds

txhx.py
```python
# AK4 Jet reshape SF
BTagSF_reshape(self, events, weights, clean_good_jets, shift, unc)
```
models.common.py
```python
self.add_systematics(
    names="btagWeight_*",
    type="shape",
    strength=1.0,
    processes=set(self.processes),
)
```

---
### TF(loose) correction
**is for QCD events and can be ignored since we are in the semi-leptonic final state**

- [ ] implemented
- [ ] in StatModel
- type S, uncorrelated, QCD multijet estimate

---
### Size of the MC samples
- [x] implemented
- [x] in StatModel
- type S, uncorrelated
- Statistical uncertainty of signal and background prediction due to limited sample size
- Technically, the implementation provided with the *combine* tool is used, configured as ```autoMCStats 10 0 1```

models.common.py
```python
lines += [f"{self.bin} autoMCStats 10 0 1"]  # defaults for last two parameters are 0 1
```

---
### Jet Pileup
- [x] implemented
- [x] in StatModel
- type S
- (not listed in AN)

txhx.py
```python
# jetPUId
if "jetpuid" in self.corrections:
    self.corrections["jetpuid"]["loose"](clean_good_jets, weights)
```
models.commmon.py
```python
self.add_systematics(
    names="jet_PUid_*",
    type="shape",
    strength=1.0,
    processes=set(self.processes),
)
```