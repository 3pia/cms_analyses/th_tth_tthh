# coding: utf-8

from utils.processes import Process, ProcessesMixin, classproperty
from .common import StatModelBase


# Add processes
class Processes(ProcessesMixin):
    """
    Background Processes
    They have to be > 0
    Signal Processes <=0
    """

    @classproperty
    def _processes(cls):
        return {
            Process(name="ttH", id=-2),
            Process(name="THQ_ctcvcp_4f_Hincl", id=-1),
            Process(name="THW_ctcvcp_5f_Hincl", id=0),
            Process(name="st", id=12),
            # Process(name="tt", id=13),
            Process(name="tt+b", id=14),
            Process(name="tt+2b", id=15),
            Process(name="tt+bb", id=16),
            Process(name="tt+lf", id=17),
            Process(name="tt+cc", id=18),
            Process(name="ttZ", id=19),
            Process(name="ttW", id=20),
            Process(name="ttVV", id=21),
            Process(name="vv", id=22),
            Process(name="vvv", id=23),
            Process(name="wjets", id=24),
            Process(name="dy", id=25),
        }


class StatModel(Processes, StatModelBase):
    pass


class StatModelNoSysts(Processes):
    @property
    def custom_lines(self):
        return [f"{self.bin} autoMCStats 10"]

    def build_systematics(self):
        return


class StatModelStatOnly(Processes):
    @property
    def custom_lines(self):
        return []

    def build_systematics(self):
        return


class Run3StatModel(StatModel):
    lumi = 300_000  # 300 fb-1


class HLLHCStatModel(StatModel):
    lumi = 3_000_000  # 3 ab-1
