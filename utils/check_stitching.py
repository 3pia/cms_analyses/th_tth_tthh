from coffea.util import load

# tt_subregions = ["tt+bb", "tt+2b", "tt+b", "tt+cc", "tt+lf"]
# tt_subdatasets = ["TTToHadronic", "TTToSemiLeptonic", "TTTo2L2Nu"]

# xsecs = {
#     "TTToHadronic": 377.9607353256,
#     "TTToSemiLeptonic": 365.45736134879996,
#     "TTTo2L2Nu": 88.34190332559997,
# }
# xsecs_tt = sum(xsecs.values())
# lumi = 59740.0  # from 2018 config

# # path_grouping = "/net/scratch/cms/dihiggs/store/th_tth_tthh/Run2_pp_13TeV_2018/GroupCoffeaProcesses/test2/txhx/39c2685922aee76b68e4f53c80032e6dcbb7ce876b16645b897c23ad19b73c32/nlo/StatModel/hists_reorder_test.coffea"
# # path_data = "/net/scratch/cms/dihiggs/store/th_tth_tthh/Run2_pp_13TeV_2018/CoffeaProcessor/test2/txhx/th_tth_tthh/txhx/Processor/explorative/data.coffea"
# path_grouping = "/net/scratch/cms/dihiggs/store/th_tth_tthh/Run2_pp_13TeV_2018/GroupCoffeaProcesses/20230331_4FS_merged/txhx/f4c6db643d775117f4ea5c203c2f9edce12e5fb2185a8516614048f375104d1e/nlo/StatModel/hists_reorder_test.coffea"
# path_data = "/net/scratch/cms/dihiggs/store/th_tth_tthh/Run2_pp_13TeV_2018/CoffeaProcessor/20230331_4FS_merged/txhx/th_tth_tthh/txhx/Processor/data.coffea"

# data = load(path_data)
# group = load(path_grouping)

# sum_gen_weights = data["sum_gen_weights"]
# sum_gen_weights_regions = sum([sum_gen_weights[(r, "nominal")] for r in tt_subregions])
# sum_gen_weights_datasets = sum([sum_gen_weights[(d, "nominal")] for d in tt_subdatasets])
# print(
#     f"\nsum gen weights regions: {sum_gen_weights_regions}\nsum gen weights datasets: {sum_gen_weights_datasets}"
# )
# print("\nBranching ratios")
# for d in tt_subdatasets:
#     print(sum_gen_weights[(d, "nominal")] / sum_gen_weights_datasets, xsecs[d] / xsecs_tt)


# pvn_data = data["histograms"]["PV_N"].hist.to_hist()
# pvn_group = group["PV_N"]

# sum_datasets = 0
# for d in tt_subdatasets:
#     sum_datasets += pvn_data[d, "inclusive", "nominal", :].sum().value

# sum_regions = 0
# for r in tt_subregions:
#     sum_regions += pvn_data[r, "inclusive", "nominal", :].sum().value

# print("\nHistograms after Processor, before group.py")
# print(
#     f"sum over subregions {tt_subregions}\n{sum_regions}\n",
#     f"sum over subdatasets {tt_subdatasets}\n{sum_datasets}",
# )

# sum_datasets = pvn_group["tt", "inclusive", "nominal", :].sum().value

# sum_regions = 0
# for r in tt_subregions:
#     sum_regions += pvn_group[r, "inclusive", "nominal", :].sum().value

# print("\nHistograms after group.py")
# print(
#     f"sum over subregions {tt_subregions}\n{sum_regions}\n",
#     f"sum over subdatasets {tt_subdatasets}\n{sum_datasets}",
# )

# # what happens in group.py
# sum_regions = 0
# for r in tt_subregions:
#     xsec_factor = sum_gen_weights[(r, "nominal")] / sum_gen_weights_regions
#     sum_regions += (
#         pvn_data[r, "inclusive", "nominal", :].sum().value
#         * xsecs_tt
#         * lumi
#         / sum_gen_weights[(r, "nominal")]
#         * xsec_factor
#     )

# sub_yield = {k: 0 for k in tt_subdatasets}
# for d in tt_subdatasets:
#     sub_yield[d] = (
#         pvn_data[d, "inclusive", "nominal", :].sum().value
#         * xsecs[d]
#         * lumi
#         / sum_gen_weights[(d, "nominal")]
#     )
# sum_datasets = sum(sub_yield.values())
# print("\ncalculations in group.py")
# print(
#     f"sum over subregions {tt_subregions}\n{sum_regions}\n",
#     f"sum over subdatasets {tt_subdatasets}\n{sum_datasets}",
# )


# # %%

# from coffea.util import load

# tt_subregions = ["tt+bb", "tt+2b", "tt+b", "tt+cc", "tt+lf"]
# tt_subdatasets = ["TTToHadronic", "TTToSemiLeptonic", "TTTo2L2Nu"]
# lumi = 59740.0
# xsecs = {
#     "TTToHadronic": 377.9607353256,
#     "TTToSemiLeptonic": 365.45736134879996,
#     "TTTo2L2Nu": 88.34190332559997,
# }

# # group = load(
# #     "/net/scratch/cms/dihiggs/store/th_tth_tthh/Run2_pp_13TeV_2018/GroupCoffeaProcesses/test2/txhx/39c2685922aee76b68e4f53c80032e6dcbb7ce876b16645b897c23ad19b73c32/nlo/StatModel/hists_reorder_test.coffea"
# # )
# # data = load(
# #     "/net/scratch/cms/dihiggs/store/th_tth_tthh/Run2_pp_13TeV_2018/CoffeaProcessor/test2/txhx/th_tth_tthh/txhx/Processor/explorative/data.coffea"
# # )
# group = load(
#     "/net/scratch/cms/dihiggs/store/th_tth_tthh/Run2_pp_13TeV_2018/GroupCoffeaProcesses/test3/txhx/39c2685922aee76b68e4f53c80032e6dcbb7ce876b16645b897c23ad19b73c32/nlo/StatModel/hists_reorder_test.coffea"
# )
# data = load(
#     "/net/scratch/cms/dihiggs/store/th_tth_tthh/Run2_pp_13TeV_2018/CoffeaProcessor/test3/txhx/th_tth_tthh/txhx/Processor/explorative/data.coffea"
# )

# sum_gen_weights = data["sum_gen_weights"]
# histograms = data["histograms"]

# pvn_data = histograms["PV_N"].hist.to_hist()
# pvn_group = group["PV_N"]

# pvn_data_copy = pvn_data.copy()
# hview = pvn_data_copy.view(True)
# sum_tt = 0
# for dataset in tt_subdatasets:
#     d = dataset.strip("TTTo")
#     sum_gen_weights_total = sum([sum_gen_weights[(f"{r}_{d}", "nominal")] for r in tt_subregions])
#     for subregion in tt_subregions:
#         name = f"{subregion}_{d}"

#         lumi_rw = xsecs[dataset] * lumi / sum_gen_weights[(name, "nominal")]

#         xsec_rw = sum_gen_weights[(name, "nominal")] / sum_gen_weights_total

#         rw = lumi_rw * xsec_rw

#         print(pvn_data_copy[name, "inclusive", "nominal", :].sum())

#         hview_r = hview[pvn_data_copy.axes["dataset"].index(name), :, 0, :]
#         hview_r["value"] *= rw
#         hview_r["variance"] *= rw**2

#         print(
#             name,
#             pvn_data_copy[name, "inclusive", "nominal", :].sum(),
#             lumi_rw,
#             xsec_rw,
#             rw,
#         )

#         sum_tt += pvn_data_copy[name, "inclusive", "nominal", :].sum().value
# print(sum_tt)
# print(pvn_group["tt", "inclusive", "nominal", :].sum())
# print(sum([pvn_group[region, "inclusive", "nominal", :].sum().value for region in tt_subregions]))

# from IPython import embed

# embed()

# # %% with merged $FS

# # for hist in hists ....
# hist = pvn_data.copy()
# hist_view = hist.view(True)
# for dataset in tt_subdatasets:
#     final_state = dataset.strip("TTTo")
#     sum_gen_weights_total = sum(
#         [sum_gen_weights[(f"{region}_{final_state}", "nominal")] for region in tt_subregions]
#     )

#     for region in tt_subregions:
#         dataset_name = f"{subregion}_{d}"

# %% ultimate check
# if you use the 5FS sample only the yield in the histograms has to be the same
print("\n### this is important ###\n")

print("sum_gen_weights")
data = load(
    "/net/scratch/cms/dihiggs/store/th_tth_tthh/Run2_pp_13TeV_2018/CoffeaProcessor/test_bg_splitting/txhx/th_tth_tthh/txhx/Processor/data.coffea"
)
hists = data["histograms"]
pvn_d = hists["PV_N"].hist.to_hist()
print("TTToSemiLeptonic: ", pvn_d["TTToSemiLeptonic", "inclusive", "nominal", :].sum().value)
tt_subr = ["tt+bb", "tt+cc", "tt+b", "tt+2b", "tt+lf"]
s = 0
for r in tt_subr:
    s += pvn_d[f"{r}_SemiLeptonic", "inclusive", "nominal", :].sum().value
print("added subregions in SemiLeptonic", s)

print("### from grouping output, yield has to be equal ###")

group = load(
    "/net/scratch/cms/dihiggs/store/th_tth_tthh/Run2_pp_13TeV_2018/GroupCoffeaProcesses/test_bg_splitting/txhx/39c2685922aee76b68e4f53c80032e6dcbb7ce876b16645b897c23ad19b73c32/nlo/StatModel/hists_reorder_test.coffea"
)

for variable in ["PV_N", "MET_phi", "Lep_pt", "Lep_eta", "Jets_pt", "Jet1_pt"]:
    hist = group[variable]
    print(variable)
    print("tt dataset: ", hist["tt", "inclusive", "nominal", :].sum())
    s = 0
    for r in tt_subr:
        s += hist[f"{r}", "inclusive", "nominal", :].sum().value
    print("added subregions tt: ", s)
