#!/bin/bash

# to execute:
# chmod +x plots_4_strats.sh
# ./plots_4_strats.sh VERSION_NAME COMMAND_TYPE

    # ./plots_4_strats.sh VERSION_NAME plots to run only PlotProducer
    # ./plots_4_strats.sh VERSION_NAME datacards to run only DatacardProducer
    # ./plots_4_strats.sh VERSION_NAME both to run both
    # ./plots_4_strats.sh VERSION_NAME (with no second argument) to default to running both commands

# Check if the version argument is provided
if [ -z "$1" ]; then
  echo "Please provide a version as argument (first)"
  exit 1
fi

# Define the version for PlotProducer
VERSION=$1

# Define the command type (plots, datacards, or both)
COMMAND_TYPE=${2:-both}

# Define the binning strategies in an array
BINNING_STRATEGIES=("equal_frequency_bkg" "equal_frequency_sig" "equal_width" "threshold")

# Define the variables in an array
VARIABLES=(
 'bJets_Minv-ClosestDR'
 'bJets_Minv-MaximumDR'
 'bJets_MinvBB-leadingbtag12'
 'bJets_MinvBB-leadingbtag23'
 'bJets_MinvBB-leadingpt12'
 'bJets_MinvBB-leadingpt23'
 'dnn_score_max'
 )

# Loop through the binning strategies and execute the commands
for STRATEGY in "${BINNING_STRATEGIES[@]}"; do
  if [ "$COMMAND_TYPE" == "plots" ] || [ "$COMMAND_TYPE" == "both" ]; then
    law run PlotProducer --version "$VERSION" --recipe txhx --year 2018 --log-scale --binning-strategy "$STRATEGY" --n-parallel 20 --remove-output 0,a,1
  fi

  if [ "$COMMAND_TYPE" == "datacards" ] || [ "$COMMAND_TYPE" == "both" ]; then
    # Loop through the variables for DatacardProducer
    for VAR in "${VARIABLES[@]}"; do
      law run DatacardProducer --version "$VERSION" --recipe txhx --category ttH --variable "$VAR" --year 2018 --binning-strategy "$STRATEGY" --transfer
    done
  fi
done
